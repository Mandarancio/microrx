# MicroRX

*Reactive principles for embedded C.*  
Asynchronous and parallel streams are a very useful and powerful way to represent a data pipeline and particularly useful for embedded systems.
All the work is represented by a pipeline that exchange message and produce a final results. Graphically could be represented as follow:
```
INPUT -------0----0----->
             |    |
FUNC1 -------V----V----->
             |    |     
FUNC2 -------V----V----->
             |    |
OUTPUT-------X----X----->
```
Multiple streams source can be defined in parallel, those sources can be regular timers (e.g. trigger an event every *N* us) or hardware interrupt (e.g. trigger an event at the rise-up of pin *N*).
The stream can be *filtered*, *merged* and *mapped* using the base blocks, creating a complex pipeline.

The [API](doc/API.md)

## Example
A simple example:

```c
#include "microrx.h"
#include "basics.h"

enum bool check_div3(uint32_t val) 
{
    return val % 3 == 0;
}

int main() 
{   
    init();
    uint32_t one = 1;
    printstr( // print the output
        filter( // filter only number divisible by 3
            accumulate( // accumulate the value
                constant( // emit 1 at every tick
                    timer(100000),  // emit a tick every 100ms 
                &one, u32)
            ), 
        &check_div3)
    );
    loop(1);
    return 0;
}
```
This code will produce the following behaviour:
```
 timer :---x---x---x---x--->
           |   |   |   |
 const :---1---1---1---1--->
           |   |   |   |
 accum :---1---2---3---4--->
           |   |   |   |
 filtr :-----------3------->
                   | >> "3"
 print :-----------3------->
 
```
 
### Weather Station
 
A more concrete example is a weather (meteo) station, for example one having a 
wind speed and direction sensor, rain (mm) sensor, humidity, temperature and 
pressure sensor.

The diagram of such system would look like this:

![schema](doc/meteo_station.png)


 
Using `MicroRX` this will be code:
 
```c
#include "microrx.h"
#include "basics.h"

// Some time constants
#define s * 1000000
#define h 60 * 60 s

// Pin identifiers
#define PIN_WSPEED 13
#define PIN_RAIN   14

// Conversion constants
#define SPEED_K    0.133
#define RAIN_K     1.5

float read_temp();
void  post_temp(float data);

float read_humi();
void  post_humi(float data);

float read_pres();
void  post_pres(float data);

int   read_wdir();
void  post_wdir(float data);

void  post_wspe(float data);
void  post_rain(float data);

int main() 
{   
    init();
    struct stream * tmr_1m = timer(60 s)
    map(mean(buffer_n(map(tmr_1m, &read_temp, f32), 60), &post_temp, NONE);
    map(mean(buffer_n(map(tmr_1m, &read_humi, f32), 60), &post_humi, NONE);
    map(mean(buffer_n(map(tmr_1m, &read_pres, f32), 60), &post_pres, NONE);
    map(mean(buffer_n(map(tmr_1m, &read_wdir, i32), 60), &post_wdir, NONE);
    
    map(mul(count_t(hw_interrupt(RISE, PIN_WSPEED), 1 h), SPEED_K), &post_wspe,
        NONE);
    map(mul(count_t(hw_interrupt(RISE, PIN_RAIN), 1 h), RAIN_K), &post_rain,
        NONE);
    
    loop(1);
    return 0;
}
 
```
