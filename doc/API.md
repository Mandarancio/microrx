## API

I tried to keep the API as simple as possible and keep the complexity hidden in the sources files.  
The main functionallities are provided by `microrx.h`, however the typing system is implemented in the `dyntype.h`.  
The basic blocks and more common operators are defined in `basics.h`.   
Some logging API will be implemented in `log.h`.

### `dyntype.h`
 
 - `enum dtype` is an enumerator defining the supported data types.
 - `enum bool` is a boolean
    - `false = 0`
    - `true = 1`
 - `struct pair` represent a pair of value (useful for mergin 2 streams)
    - `void * left` is the left value
    - `dtype l_type` is the type of the the left value
    - `void * right` is the right value
    - `dtype r_type` is the type of the the right value
 - `struct list` represent a concatenated list of value
    - `void * value` the value of the current element
    - `dtype type` the type of the value
    - `list * next` the next element of the list
 
### `microrx.h`

 - `enum stype` is an enumerator defining the type of stream:
    - `SW_STREAM` is a standard software stream, it always produce a result when is triggered
    - `HW_STREAM` is a hardware stream, representing interrupts
    - `CH_STREAM` is a software stream with a boolean flag, it only produce a result when some conditions are met.
 - `struct stream` is the more important element and represent a stream
    - `uint32_t id` is the unique id of the stream
    - `dtype input` is the input type
    - `dtype output` is the output type
    - `stype type` is the type of stream
    - `void * function` is the function called to process the stream
    - `void * payload` is a pointer to optional payload 
