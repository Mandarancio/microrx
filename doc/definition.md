# Implementation

```c
int main() {
    // initialize microrx extension
    init();
     
    //Stream declaration
    
    stream * s_acc = accumulate(constant(timer(100), 1));
    // tick every 0.1ms
    // emit 1 every tick
    // increase the val
    
    stream * s1 = map(s_acc, cos, f32);
    // map fn cos to s_acc
    
    stream * s2 = map(s_acc, sinc, f32);
    // map fn sinc to s_acc
    
    stream * sm = map(merge(s1,s2), max, u8);
    // merge s1 and s2 -> [[s1.val, s2.val]]
    // map fn max to the pair stream
    
    stream * _v = printstr(sm);
    // print result
    
    // execution loop
    loop(100); //minimum tick is 1ms
    return 0;
}
```