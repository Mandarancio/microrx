#ifndef SMATH_H
#define SMATH_H

#include "microrx.h"

Stream * average_t (Stream * str);
Stream * average_n (Stream * str);

Stream * mean      (Stream * str);

Stream * sum_k     (Stream * str,
                    void   * constant,
                    dtype    type);
Stream * sub_k     (Stream * str,
                    void   * constant,
                    dtype    type);
Stream * mul_k     (Stream * str,
                    void   * constant,
                    dtype    type);
Stream * div_k     (Stream * str,
                    void   * constant,
                    dtype    type);

Stream * sum       (Stream * lho,
                    Stream * rho);
Stream * sub       (Stream * lho,
                    Stream * rho);
Stream * mul       (Stream * lho,
                    Stream * rho);
Stream * div       (Stream * lho,
                    Stream * rho);
#endif
