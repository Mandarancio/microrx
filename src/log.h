/**
 * \file log.h
 * \brief Console logging utilities
 * \author Martino Ferrari
 */
#ifndef LOG_H
#define LOG_H


struct logger {
  void (*log)(char *msg,...);
  void (*warning)(char *msg,...);
  void (*error)(char *msg,...);
};

struct logger * stdio_logger();

#endif
