#include "basics.h"
#include <stdio.h>
#include <stdlib.h>


/***
 * AUTO GENERATED CODE
 * gen_print.py
 ***/

Message * _print_any(Stream *str, Message *msg)
{
  printf("??: %p\n", msg->payload);
  Message * out = new(Message);
  out->payload = msg->payload;
  out->type = msg->type;
  out->source = str;
  return out;
}

Message * _print_i8(Stream *str, Message *msg)
{
  printf("i8: %d\n", *(int8_t*)msg->payload);
  Message * out = new(Message);
  out->payload = msg->payload;
  out->source = str;
  out->type = msg->type;
  return out;
}

Message * _print_i16(Stream *str, Message *msg)
{
  printf("i16: %d\n", *(int16_t*)msg->payload);
  Message * out = new(Message);
  out->payload = msg->payload;
  out->source = str;
  out->type = msg->type;
  return out;
}

Message * _print_i32(Stream *str, Message *msg)
{
  printf("i32: %d\n", *(int32_t*)msg->payload);
  Message * out = new(Message);
  out->payload = msg->payload;
  out->source = str;
  out->type = msg->type;
  return out;
}

Message * _print_i64(Stream *str, Message *msg)
{
  printf("i64: %ld\n", *(int64_t*)msg->payload);
  Message * out = new(Message);
  out->payload = msg->payload;
  out->source = str;
  out->type = msg->type;
  return out;
}

Message * _print_u8(Stream *str, Message *msg)
{
  printf("u8: %u\n", *(uint8_t*)msg->payload);
  Message * out = new(Message);
  out->payload = msg->payload;
  out->source = str;
  out->type = msg->type;
  return out;
}

Message * _print_u16(Stream *str, Message *msg)
{
  printf("u16: %u\n", *(uint16_t*)msg->payload);
  Message * out = new(Message);
  out->payload = msg->payload;
  out->source = str;
  out->type = msg->type;
  return out;
}

Message * _print_u32(Stream *str, Message *msg)
{
  printf("u32: %u\n", *(uint32_t*)msg->payload);
  Message * out = new(Message);
  out->payload = msg->payload;
  out->source = str;
  out->type = msg->type;
  return out;
}

Message * _print_u64(Stream *str, Message *msg)
{
  printf("u64: %lu\n", *(uint64_t*)msg->payload);
  Message * out = new(Message);
  out->payload = msg->payload;
  out->source = str;
  out->type = msg->type;
  return out;
}

Message * _print_f32(Stream *str, Message *msg)
{
  printf("f32: %f\n", *(float*)msg->payload);
  Message * out = new(Message);
  out->payload = msg->payload;
  out->source = str;
  out->type = msg->type;
  return out;
}

Message * _print_f64(Stream *str, Message *msg)
{
  printf("f64: %f\n", *(double*)msg->payload);
  Message * out = new(Message);
  out->payload = msg->payload;
  out->source = str;
  out->type = msg->type;
  return out;
}

Message * _print_BOOL(Stream *str, Message *msg)
{
  printf("BOOL: %u\n", *(bool*)msg->payload);
  Message * out = new(Message);
  out->payload = msg->payload;
  out->source = str;
  out->type = msg->type;
  return out;
}

Message * _print_CHAR(Stream *str, Message *msg)
{
  printf("CHAR: %c\n", *(char*)msg->payload);
  Message * out = new(Message);
  out->payload = msg->payload;
  out->source = str;
  out->type = msg->type;
  return out;
}

Stream * printstr(Stream *str)
{
  Stream *target;
  Schedule s;
  s.type = ON_MESSAGE;
  switch (str->output) {
  case i8: 
    target = add_stream(str->output, str->output, s, &_print_i8, 0);
    break;
  case i16: 
    target = add_stream(str->output, str->output, s, &_print_i16, 0);
    break;
  case i32: 
    target = add_stream(str->output, str->output, s, &_print_i32, 0);
    break;
  case i64: 
    target = add_stream(str->output, str->output, s, &_print_i64, 0);
    break;
  case u8: 
    target = add_stream(str->output, str->output, s, &_print_u8, 0);
    break;
  case u16: 
    target = add_stream(str->output, str->output, s, &_print_u16, 0);
    break;
  case u32: 
    target = add_stream(str->output, str->output, s, &_print_u32, 0);
    break;
  case u64: 
    target = add_stream(str->output, str->output, s, &_print_u64, 0);
    break;
  case f32: 
    target = add_stream(str->output, str->output, s, &_print_f32, 0);
    break;
  case f64: 
    target = add_stream(str->output, str->output, s, &_print_f64, 0);
    break;
  case BOOL: 
    target = add_stream(str->output, str->output, s, &_print_BOOL, 0);
    break;
  case CHAR: 
    target = add_stream(str->output, str->output, s, &_print_CHAR, 0);
    break;
  default:
    target = add_stream(str->output, str->output, s, &_print_any, 0);
    break;
  }
  connect(str, target);
  return target;
}
