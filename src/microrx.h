/**
 * \file microrx.h
 * \brief Core functionality of MicroRX
 * \author Martino Ferrari
 *
 * Core functionality of MicroRX
 *
 */
#ifndef MICRORX_H
#define MICRORX_H

#include <stdint.h>
#include "dyntype.h"

#define new(T) (T*)malloc(sizeof(T))
#define new_n(T, n) (T*)malloc(sizeof(T)*n)
#define resize(p, T, n) (T*)realloc(p, sizeof(T)*n)

typedef uint64_t time;
typedef enum _stype_ stype;
typedef struct _message_ Message;
typedef struct _stream_ Stream;
typedef enum _scheduling_ sched_type;
typedef struct _schedule_ Schedule;

void init();
int  loop();

enum _stype_ {
  HW_SOURCE, // Hardware stream, such as an interupt
  SW_STREAM, // Software stream with an additional boolean flag;
};

enum _scheduling_{
  ON_TIMER,
  ON_MESSAGE,
  ON_HWEVENT,
  NEVER
};

/**
 * \struct message
 */
struct _message_ {
  time        timestamp;
  Stream    * source;
  void      * payload;
  dtype       type;
  Message   * next;
};

struct _schedule_ {
  sched_type type;
  time       interval;
  time       previous_call;
};


struct _stream_ {
  uint32_t    id;
  dtype       input;
  dtype       output;
  Schedule    schedule;
  Message * (*callback)(Stream  * self,
                        Message * input);
  void      * payload;
};


Stream * add_stream (dtype      input,
                     dtype      output,
                     Schedule   schedule,
                     Message *(*callback)(Stream  * self,
                                          Message * input),
                     void   *   payload);

bool connect        (Stream *  source,
                     Stream *  target);

#endif

