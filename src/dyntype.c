#include "dyntype.h"
#include <stdlib.h>

List * push_back(List * head, void * value, dtype type)
{
  List * el = malloc(sizeof(List));
  el->next = 0;
  el->value = value;
  el->type = type;
  if (!head)
    return el;
  List * prev = head;
  while (prev->next)
    prev = prev->next;
  prev->next = el;
  return head;
}

void insert_u8(DynArray *self,
               uint8_t  *val)
{
  size_t size = sizeof(uint8_t) * (++self->size);
  self->array = realloc (self->array, size);
  ((uint8_t *)self->array)[self->size-1] = *val;
}

void insert_u16(DynArray *self,
                uint16_t  *val)
{
  size_t size = sizeof(uint16_t) * (++self->size);
  self->array = realloc (self->array, size);
  ((uint16_t *)self->array)[self->size-1] = *val;
}

void insert_u32(DynArray *self,
                uint32_t  *val)
{
  size_t size = sizeof(uint32_t) * (++self->size);
  self->array = realloc (self->array, size);
  ((uint32_t *)self->array)[self->size-1] = *val;
}

void insert_u64(DynArray *self,
                uint64_t  *val)
{
  size_t size = sizeof(uint64_t) * (++self->size);
  self->array = realloc (self->array, size);
  ((uint64_t *)self->array)[self->size-1] = *val;
}


void insert_i8(DynArray *self,
               int8_t  *val)
{
  size_t size = sizeof(int8_t) * (++self->size);
  self->array = realloc (self->array, size);
  ((int8_t *)self->array)[self->size-1] = *val;
}

void insert_i16(DynArray *self,
                int16_t  *val)
{
  size_t size = sizeof(int16_t) * (++self->size);
  self->array = realloc (self->array, size);
  ((int16_t *)self->array)[self->size-1] = *val;
}

void insert_i32(DynArray *self,
                int32_t  *val)
{
  size_t size = sizeof(int32_t) * (++self->size);
  self->array = realloc (self->array, size);
  ((int32_t *)self->array)[self->size-1] = *val;
}

void insert_i64(DynArray *self,
                int64_t  *val)
{
  size_t size = sizeof(int64_t) * (++self->size);
  self->array = realloc (self->array, size);
  ((int64_t *)self->array)[self->size-1] = *val;
}

DynArray * insert(DynArray * self,
                  void     * value)
{
  switch (self->type) {
  case u8:
    insert_u8  (self, value);
    break;
  case u16:
    insert_u16 (self, value);
    break;
  case u32:
    insert_u32 (self, value);
    break;
  case u64:
    insert_u64 (self, value);
    break;
  case i8:
    insert_i8  (self, value);
    break;
  case i16:
    insert_i16 (self, value);
    break;
  case i32:
    insert_i32 (self, value);
    break;
  case i64:
    insert_i64 (self, value);
    break;
  default:
    break;
  }
  return self;
}

