#include "basics.h"
#include <stdlib.h>


/***
 * AUTO GENERATED CODE
 * gen_convert.py
 ***/

Message * _conv_i8_i8(Stream * str, Message *msg)

{
  int8_t * val = new(int8_t);
  *val = (int8_t)*(int8_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i8_i16(Stream * str, Message *msg)

{
  int16_t * val = new(int16_t);
  *val = (int16_t)*(int8_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i8_i32(Stream * str, Message *msg)

{
  int32_t * val = new(int32_t);
  *val = (int32_t)*(int8_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i8_i64(Stream * str, Message *msg)

{
  int64_t * val = new(int64_t);
  *val = (int64_t)*(int8_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i8_u8(Stream * str, Message *msg)

{
  uint8_t * val = new(uint8_t);
  *val = (uint8_t)*(int8_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i8_u16(Stream * str, Message *msg)

{
  uint16_t * val = new(uint16_t);
  *val = (uint16_t)*(int8_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i8_u32(Stream * str, Message *msg)

{
  uint32_t * val = new(uint32_t);
  *val = (uint32_t)*(int8_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i8_u64(Stream * str, Message *msg)

{
  uint64_t * val = new(uint64_t);
  *val = (uint64_t)*(int8_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i8_f32(Stream * str, Message *msg)

{
  float * val = new(float);
  *val = (float)*(int8_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i8_f64(Stream * str, Message *msg)

{
  double * val = new(double);
  *val = (double)*(int8_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i8_BOOL(Stream * str, Message *msg)

{
  bool * val = new(bool);
  *val = (bool)*(int8_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i8_CHAR(Stream * str, Message *msg)

{
  char * val = new(char);
  *val = (char)*(int8_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

void * _get_i8(dtype out)
{
  switch(out) {
  case i8:
    return &_conv_i8_i8;
  case i16:
    return &_conv_i8_i16;
  case i32:
    return &_conv_i8_i32;
  case i64:
    return &_conv_i8_i64;
  case u8:
    return &_conv_i8_u8;
  case u16:
    return &_conv_i8_u16;
  case u32:
    return &_conv_i8_u32;
  case u64:
    return &_conv_i8_u64;
  case f32:
    return &_conv_i8_f32;
  case f64:
    return &_conv_i8_f64;
  case BOOL:
    return &_conv_i8_BOOL;
  case CHAR:
    return &_conv_i8_CHAR;
  default: return 0;
  }
}

Message * _conv_i16_i8(Stream * str, Message *msg)

{
  int8_t * val = new(int8_t);
  *val = (int8_t)*(int16_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i16_i16(Stream * str, Message *msg)

{
  int16_t * val = new(int16_t);
  *val = (int16_t)*(int16_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i16_i32(Stream * str, Message *msg)

{
  int32_t * val = new(int32_t);
  *val = (int32_t)*(int16_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i16_i64(Stream * str, Message *msg)

{
  int64_t * val = new(int64_t);
  *val = (int64_t)*(int16_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i16_u8(Stream * str, Message *msg)

{
  uint8_t * val = new(uint8_t);
  *val = (uint8_t)*(int16_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i16_u16(Stream * str, Message *msg)

{
  uint16_t * val = new(uint16_t);
  *val = (uint16_t)*(int16_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i16_u32(Stream * str, Message *msg)

{
  uint32_t * val = new(uint32_t);
  *val = (uint32_t)*(int16_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i16_u64(Stream * str, Message *msg)

{
  uint64_t * val = new(uint64_t);
  *val = (uint64_t)*(int16_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i16_f32(Stream * str, Message *msg)

{
  float * val = new(float);
  *val = (float)*(int16_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i16_f64(Stream * str, Message *msg)

{
  double * val = new(double);
  *val = (double)*(int16_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i16_BOOL(Stream * str, Message *msg)

{
  bool * val = new(bool);
  *val = (bool)*(int16_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i16_CHAR(Stream * str, Message *msg)

{
  char * val = new(char);
  *val = (char)*(int16_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

void * _get_i16(dtype out)
{
  switch(out) {
  case i8:
    return &_conv_i16_i8;
  case i16:
    return &_conv_i16_i16;
  case i32:
    return &_conv_i16_i32;
  case i64:
    return &_conv_i16_i64;
  case u8:
    return &_conv_i16_u8;
  case u16:
    return &_conv_i16_u16;
  case u32:
    return &_conv_i16_u32;
  case u64:
    return &_conv_i16_u64;
  case f32:
    return &_conv_i16_f32;
  case f64:
    return &_conv_i16_f64;
  case BOOL:
    return &_conv_i16_BOOL;
  case CHAR:
    return &_conv_i16_CHAR;
  default: return 0;
  }
}

Message * _conv_i32_i8(Stream * str, Message *msg)

{
  int8_t * val = new(int8_t);
  *val = (int8_t)*(int32_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i32_i16(Stream * str, Message *msg)

{
  int16_t * val = new(int16_t);
  *val = (int16_t)*(int32_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i32_i32(Stream * str, Message *msg)

{
  int32_t * val = new(int32_t);
  *val = (int32_t)*(int32_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i32_i64(Stream * str, Message *msg)

{
  int64_t * val = new(int64_t);
  *val = (int64_t)*(int32_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i32_u8(Stream * str, Message *msg)

{
  uint8_t * val = new(uint8_t);
  *val = (uint8_t)*(int32_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i32_u16(Stream * str, Message *msg)

{
  uint16_t * val = new(uint16_t);
  *val = (uint16_t)*(int32_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i32_u32(Stream * str, Message *msg)

{
  uint32_t * val = new(uint32_t);
  *val = (uint32_t)*(int32_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i32_u64(Stream * str, Message *msg)

{
  uint64_t * val = new(uint64_t);
  *val = (uint64_t)*(int32_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i32_f32(Stream * str, Message *msg)

{
  float * val = new(float);
  *val = (float)*(int32_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i32_f64(Stream * str, Message *msg)

{
  double * val = new(double);
  *val = (double)*(int32_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i32_BOOL(Stream * str, Message *msg)

{
  bool * val = new(bool);
  *val = (bool)*(int32_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i32_CHAR(Stream * str, Message *msg)

{
  char * val = new(char);
  *val = (char)*(int32_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

void * _get_i32(dtype out)
{
  switch(out) {
  case i8:
    return &_conv_i32_i8;
  case i16:
    return &_conv_i32_i16;
  case i32:
    return &_conv_i32_i32;
  case i64:
    return &_conv_i32_i64;
  case u8:
    return &_conv_i32_u8;
  case u16:
    return &_conv_i32_u16;
  case u32:
    return &_conv_i32_u32;
  case u64:
    return &_conv_i32_u64;
  case f32:
    return &_conv_i32_f32;
  case f64:
    return &_conv_i32_f64;
  case BOOL:
    return &_conv_i32_BOOL;
  case CHAR:
    return &_conv_i32_CHAR;
  default: return 0;
  }
}

Message * _conv_i64_i8(Stream * str, Message *msg)

{
  int8_t * val = new(int8_t);
  *val = (int8_t)*(int64_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i64_i16(Stream * str, Message *msg)

{
  int16_t * val = new(int16_t);
  *val = (int16_t)*(int64_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i64_i32(Stream * str, Message *msg)

{
  int32_t * val = new(int32_t);
  *val = (int32_t)*(int64_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i64_i64(Stream * str, Message *msg)

{
  int64_t * val = new(int64_t);
  *val = (int64_t)*(int64_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i64_u8(Stream * str, Message *msg)

{
  uint8_t * val = new(uint8_t);
  *val = (uint8_t)*(int64_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i64_u16(Stream * str, Message *msg)

{
  uint16_t * val = new(uint16_t);
  *val = (uint16_t)*(int64_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i64_u32(Stream * str, Message *msg)

{
  uint32_t * val = new(uint32_t);
  *val = (uint32_t)*(int64_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i64_u64(Stream * str, Message *msg)

{
  uint64_t * val = new(uint64_t);
  *val = (uint64_t)*(int64_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i64_f32(Stream * str, Message *msg)

{
  float * val = new(float);
  *val = (float)*(int64_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i64_f64(Stream * str, Message *msg)

{
  double * val = new(double);
  *val = (double)*(int64_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i64_BOOL(Stream * str, Message *msg)

{
  bool * val = new(bool);
  *val = (bool)*(int64_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_i64_CHAR(Stream * str, Message *msg)

{
  char * val = new(char);
  *val = (char)*(int64_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

void * _get_i64(dtype out)
{
  switch(out) {
  case i8:
    return &_conv_i64_i8;
  case i16:
    return &_conv_i64_i16;
  case i32:
    return &_conv_i64_i32;
  case i64:
    return &_conv_i64_i64;
  case u8:
    return &_conv_i64_u8;
  case u16:
    return &_conv_i64_u16;
  case u32:
    return &_conv_i64_u32;
  case u64:
    return &_conv_i64_u64;
  case f32:
    return &_conv_i64_f32;
  case f64:
    return &_conv_i64_f64;
  case BOOL:
    return &_conv_i64_BOOL;
  case CHAR:
    return &_conv_i64_CHAR;
  default: return 0;
  }
}

Message * _conv_u8_i8(Stream * str, Message *msg)

{
  int8_t * val = new(int8_t);
  *val = (int8_t)*(uint8_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u8_i16(Stream * str, Message *msg)

{
  int16_t * val = new(int16_t);
  *val = (int16_t)*(uint8_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u8_i32(Stream * str, Message *msg)

{
  int32_t * val = new(int32_t);
  *val = (int32_t)*(uint8_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u8_i64(Stream * str, Message *msg)

{
  int64_t * val = new(int64_t);
  *val = (int64_t)*(uint8_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u8_u8(Stream * str, Message *msg)

{
  uint8_t * val = new(uint8_t);
  *val = (uint8_t)*(uint8_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u8_u16(Stream * str, Message *msg)

{
  uint16_t * val = new(uint16_t);
  *val = (uint16_t)*(uint8_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u8_u32(Stream * str, Message *msg)

{
  uint32_t * val = new(uint32_t);
  *val = (uint32_t)*(uint8_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u8_u64(Stream * str, Message *msg)

{
  uint64_t * val = new(uint64_t);
  *val = (uint64_t)*(uint8_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u8_f32(Stream * str, Message *msg)

{
  float * val = new(float);
  *val = (float)*(uint8_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u8_f64(Stream * str, Message *msg)

{
  double * val = new(double);
  *val = (double)*(uint8_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u8_BOOL(Stream * str, Message *msg)

{
  bool * val = new(bool);
  *val = (bool)*(uint8_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u8_CHAR(Stream * str, Message *msg)

{
  char * val = new(char);
  *val = (char)*(uint8_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

void * _get_u8(dtype out)
{
  switch(out) {
  case i8:
    return &_conv_u8_i8;
  case i16:
    return &_conv_u8_i16;
  case i32:
    return &_conv_u8_i32;
  case i64:
    return &_conv_u8_i64;
  case u8:
    return &_conv_u8_u8;
  case u16:
    return &_conv_u8_u16;
  case u32:
    return &_conv_u8_u32;
  case u64:
    return &_conv_u8_u64;
  case f32:
    return &_conv_u8_f32;
  case f64:
    return &_conv_u8_f64;
  case BOOL:
    return &_conv_u8_BOOL;
  case CHAR:
    return &_conv_u8_CHAR;
  default: return 0;
  }
}

Message * _conv_u16_i8(Stream * str, Message *msg)

{
  int8_t * val = new(int8_t);
  *val = (int8_t)*(uint16_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u16_i16(Stream * str, Message *msg)

{
  int16_t * val = new(int16_t);
  *val = (int16_t)*(uint16_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u16_i32(Stream * str, Message *msg)

{
  int32_t * val = new(int32_t);
  *val = (int32_t)*(uint16_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u16_i64(Stream * str, Message *msg)

{
  int64_t * val = new(int64_t);
  *val = (int64_t)*(uint16_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u16_u8(Stream * str, Message *msg)

{
  uint8_t * val = new(uint8_t);
  *val = (uint8_t)*(uint16_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u16_u16(Stream * str, Message *msg)

{
  uint16_t * val = new(uint16_t);
  *val = (uint16_t)*(uint16_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u16_u32(Stream * str, Message *msg)

{
  uint32_t * val = new(uint32_t);
  *val = (uint32_t)*(uint16_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u16_u64(Stream * str, Message *msg)

{
  uint64_t * val = new(uint64_t);
  *val = (uint64_t)*(uint16_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u16_f32(Stream * str, Message *msg)

{
  float * val = new(float);
  *val = (float)*(uint16_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u16_f64(Stream * str, Message *msg)

{
  double * val = new(double);
  *val = (double)*(uint16_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u16_BOOL(Stream * str, Message *msg)

{
  bool * val = new(bool);
  *val = (bool)*(uint16_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u16_CHAR(Stream * str, Message *msg)

{
  char * val = new(char);
  *val = (char)*(uint16_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

void * _get_u16(dtype out)
{
  switch(out) {
  case i8:
    return &_conv_u16_i8;
  case i16:
    return &_conv_u16_i16;
  case i32:
    return &_conv_u16_i32;
  case i64:
    return &_conv_u16_i64;
  case u8:
    return &_conv_u16_u8;
  case u16:
    return &_conv_u16_u16;
  case u32:
    return &_conv_u16_u32;
  case u64:
    return &_conv_u16_u64;
  case f32:
    return &_conv_u16_f32;
  case f64:
    return &_conv_u16_f64;
  case BOOL:
    return &_conv_u16_BOOL;
  case CHAR:
    return &_conv_u16_CHAR;
  default: return 0;
  }
}

Message * _conv_u32_i8(Stream * str, Message *msg)

{
  int8_t * val = new(int8_t);
  *val = (int8_t)*(uint32_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u32_i16(Stream * str, Message *msg)

{
  int16_t * val = new(int16_t);
  *val = (int16_t)*(uint32_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u32_i32(Stream * str, Message *msg)

{
  int32_t * val = new(int32_t);
  *val = (int32_t)*(uint32_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u32_i64(Stream * str, Message *msg)

{
  int64_t * val = new(int64_t);
  *val = (int64_t)*(uint32_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u32_u8(Stream * str, Message *msg)

{
  uint8_t * val = new(uint8_t);
  *val = (uint8_t)*(uint32_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u32_u16(Stream * str, Message *msg)

{
  uint16_t * val = new(uint16_t);
  *val = (uint16_t)*(uint32_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u32_u32(Stream * str, Message *msg)

{
  uint32_t * val = new(uint32_t);
  *val = (uint32_t)*(uint32_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u32_u64(Stream * str, Message *msg)

{
  uint64_t * val = new(uint64_t);
  *val = (uint64_t)*(uint32_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u32_f32(Stream * str, Message *msg)

{
  float * val = new(float);
  *val = (float)*(uint32_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u32_f64(Stream * str, Message *msg)

{
  double * val = new(double);
  *val = (double)*(uint32_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u32_BOOL(Stream * str, Message *msg)

{
  bool * val = new(bool);
  *val = (bool)*(uint32_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u32_CHAR(Stream * str, Message *msg)

{
  char * val = new(char);
  *val = (char)*(uint32_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

void * _get_u32(dtype out)
{
  switch(out) {
  case i8:
    return &_conv_u32_i8;
  case i16:
    return &_conv_u32_i16;
  case i32:
    return &_conv_u32_i32;
  case i64:
    return &_conv_u32_i64;
  case u8:
    return &_conv_u32_u8;
  case u16:
    return &_conv_u32_u16;
  case u32:
    return &_conv_u32_u32;
  case u64:
    return &_conv_u32_u64;
  case f32:
    return &_conv_u32_f32;
  case f64:
    return &_conv_u32_f64;
  case BOOL:
    return &_conv_u32_BOOL;
  case CHAR:
    return &_conv_u32_CHAR;
  default: return 0;
  }
}

Message * _conv_u64_i8(Stream * str, Message *msg)

{
  int8_t * val = new(int8_t);
  *val = (int8_t)*(uint64_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u64_i16(Stream * str, Message *msg)

{
  int16_t * val = new(int16_t);
  *val = (int16_t)*(uint64_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u64_i32(Stream * str, Message *msg)

{
  int32_t * val = new(int32_t);
  *val = (int32_t)*(uint64_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u64_i64(Stream * str, Message *msg)

{
  int64_t * val = new(int64_t);
  *val = (int64_t)*(uint64_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u64_u8(Stream * str, Message *msg)

{
  uint8_t * val = new(uint8_t);
  *val = (uint8_t)*(uint64_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u64_u16(Stream * str, Message *msg)

{
  uint16_t * val = new(uint16_t);
  *val = (uint16_t)*(uint64_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u64_u32(Stream * str, Message *msg)

{
  uint32_t * val = new(uint32_t);
  *val = (uint32_t)*(uint64_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u64_u64(Stream * str, Message *msg)

{
  uint64_t * val = new(uint64_t);
  *val = (uint64_t)*(uint64_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u64_f32(Stream * str, Message *msg)

{
  float * val = new(float);
  *val = (float)*(uint64_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u64_f64(Stream * str, Message *msg)

{
  double * val = new(double);
  *val = (double)*(uint64_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u64_BOOL(Stream * str, Message *msg)

{
  bool * val = new(bool);
  *val = (bool)*(uint64_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_u64_CHAR(Stream * str, Message *msg)

{
  char * val = new(char);
  *val = (char)*(uint64_t*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

void * _get_u64(dtype out)
{
  switch(out) {
  case i8:
    return &_conv_u64_i8;
  case i16:
    return &_conv_u64_i16;
  case i32:
    return &_conv_u64_i32;
  case i64:
    return &_conv_u64_i64;
  case u8:
    return &_conv_u64_u8;
  case u16:
    return &_conv_u64_u16;
  case u32:
    return &_conv_u64_u32;
  case u64:
    return &_conv_u64_u64;
  case f32:
    return &_conv_u64_f32;
  case f64:
    return &_conv_u64_f64;
  case BOOL:
    return &_conv_u64_BOOL;
  case CHAR:
    return &_conv_u64_CHAR;
  default: return 0;
  }
}

Message * _conv_f32_i8(Stream * str, Message *msg)

{
  int8_t * val = new(int8_t);
  *val = (int8_t)*(float*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_f32_i16(Stream * str, Message *msg)

{
  int16_t * val = new(int16_t);
  *val = (int16_t)*(float*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_f32_i32(Stream * str, Message *msg)

{
  int32_t * val = new(int32_t);
  *val = (int32_t)*(float*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_f32_i64(Stream * str, Message *msg)

{
  int64_t * val = new(int64_t);
  *val = (int64_t)*(float*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_f32_u8(Stream * str, Message *msg)

{
  uint8_t * val = new(uint8_t);
  *val = (uint8_t)*(float*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_f32_u16(Stream * str, Message *msg)

{
  uint16_t * val = new(uint16_t);
  *val = (uint16_t)*(float*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_f32_u32(Stream * str, Message *msg)

{
  uint32_t * val = new(uint32_t);
  *val = (uint32_t)*(float*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_f32_u64(Stream * str, Message *msg)

{
  uint64_t * val = new(uint64_t);
  *val = (uint64_t)*(float*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_f32_f32(Stream * str, Message *msg)

{
  float * val = new(float);
  *val = (float)*(float*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_f32_f64(Stream * str, Message *msg)

{
  double * val = new(double);
  *val = (double)*(float*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_f32_BOOL(Stream * str, Message *msg)

{
  bool * val = new(bool);
  *val = (bool)*(float*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_f32_CHAR(Stream * str, Message *msg)

{
  char * val = new(char);
  *val = (char)*(float*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

void * _get_f32(dtype out)
{
  switch(out) {
  case i8:
    return &_conv_f32_i8;
  case i16:
    return &_conv_f32_i16;
  case i32:
    return &_conv_f32_i32;
  case i64:
    return &_conv_f32_i64;
  case u8:
    return &_conv_f32_u8;
  case u16:
    return &_conv_f32_u16;
  case u32:
    return &_conv_f32_u32;
  case u64:
    return &_conv_f32_u64;
  case f32:
    return &_conv_f32_f32;
  case f64:
    return &_conv_f32_f64;
  case BOOL:
    return &_conv_f32_BOOL;
  case CHAR:
    return &_conv_f32_CHAR;
  default: return 0;
  }
}

Message * _conv_f64_i8(Stream * str, Message *msg)

{
  int8_t * val = new(int8_t);
  *val = (int8_t)*(double*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_f64_i16(Stream * str, Message *msg)

{
  int16_t * val = new(int16_t);
  *val = (int16_t)*(double*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_f64_i32(Stream * str, Message *msg)

{
  int32_t * val = new(int32_t);
  *val = (int32_t)*(double*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_f64_i64(Stream * str, Message *msg)

{
  int64_t * val = new(int64_t);
  *val = (int64_t)*(double*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_f64_u8(Stream * str, Message *msg)

{
  uint8_t * val = new(uint8_t);
  *val = (uint8_t)*(double*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_f64_u16(Stream * str, Message *msg)

{
  uint16_t * val = new(uint16_t);
  *val = (uint16_t)*(double*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_f64_u32(Stream * str, Message *msg)

{
  uint32_t * val = new(uint32_t);
  *val = (uint32_t)*(double*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_f64_u64(Stream * str, Message *msg)

{
  uint64_t * val = new(uint64_t);
  *val = (uint64_t)*(double*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_f64_f32(Stream * str, Message *msg)

{
  float * val = new(float);
  *val = (float)*(double*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_f64_f64(Stream * str, Message *msg)

{
  double * val = new(double);
  *val = (double)*(double*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_f64_BOOL(Stream * str, Message *msg)

{
  bool * val = new(bool);
  *val = (bool)*(double*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_f64_CHAR(Stream * str, Message *msg)

{
  char * val = new(char);
  *val = (char)*(double*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

void * _get_f64(dtype out)
{
  switch(out) {
  case i8:
    return &_conv_f64_i8;
  case i16:
    return &_conv_f64_i16;
  case i32:
    return &_conv_f64_i32;
  case i64:
    return &_conv_f64_i64;
  case u8:
    return &_conv_f64_u8;
  case u16:
    return &_conv_f64_u16;
  case u32:
    return &_conv_f64_u32;
  case u64:
    return &_conv_f64_u64;
  case f32:
    return &_conv_f64_f32;
  case f64:
    return &_conv_f64_f64;
  case BOOL:
    return &_conv_f64_BOOL;
  case CHAR:
    return &_conv_f64_CHAR;
  default: return 0;
  }
}

Message * _conv_BOOL_i8(Stream * str, Message *msg)

{
  int8_t * val = new(int8_t);
  *val = (int8_t)*(bool*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_BOOL_i16(Stream * str, Message *msg)

{
  int16_t * val = new(int16_t);
  *val = (int16_t)*(bool*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_BOOL_i32(Stream * str, Message *msg)

{
  int32_t * val = new(int32_t);
  *val = (int32_t)*(bool*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_BOOL_i64(Stream * str, Message *msg)

{
  int64_t * val = new(int64_t);
  *val = (int64_t)*(bool*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_BOOL_u8(Stream * str, Message *msg)

{
  uint8_t * val = new(uint8_t);
  *val = (uint8_t)*(bool*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_BOOL_u16(Stream * str, Message *msg)

{
  uint16_t * val = new(uint16_t);
  *val = (uint16_t)*(bool*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_BOOL_u32(Stream * str, Message *msg)

{
  uint32_t * val = new(uint32_t);
  *val = (uint32_t)*(bool*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_BOOL_u64(Stream * str, Message *msg)

{
  uint64_t * val = new(uint64_t);
  *val = (uint64_t)*(bool*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_BOOL_f32(Stream * str, Message *msg)

{
  float * val = new(float);
  *val = (float)*(bool*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_BOOL_f64(Stream * str, Message *msg)

{
  double * val = new(double);
  *val = (double)*(bool*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_BOOL_BOOL(Stream * str, Message *msg)

{
  bool * val = new(bool);
  *val = (bool)*(bool*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_BOOL_CHAR(Stream * str, Message *msg)

{
  char * val = new(char);
  *val = (char)*(bool*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

void * _get_BOOL(dtype out)
{
  switch(out) {
  case i8:
    return &_conv_BOOL_i8;
  case i16:
    return &_conv_BOOL_i16;
  case i32:
    return &_conv_BOOL_i32;
  case i64:
    return &_conv_BOOL_i64;
  case u8:
    return &_conv_BOOL_u8;
  case u16:
    return &_conv_BOOL_u16;
  case u32:
    return &_conv_BOOL_u32;
  case u64:
    return &_conv_BOOL_u64;
  case f32:
    return &_conv_BOOL_f32;
  case f64:
    return &_conv_BOOL_f64;
  case BOOL:
    return &_conv_BOOL_BOOL;
  case CHAR:
    return &_conv_BOOL_CHAR;
  default: return 0;
  }
}

Message * _conv_CHAR_i8(Stream * str, Message *msg)

{
  int8_t * val = new(int8_t);
  *val = (int8_t)*(char*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_CHAR_i16(Stream * str, Message *msg)

{
  int16_t * val = new(int16_t);
  *val = (int16_t)*(char*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_CHAR_i32(Stream * str, Message *msg)

{
  int32_t * val = new(int32_t);
  *val = (int32_t)*(char*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_CHAR_i64(Stream * str, Message *msg)

{
  int64_t * val = new(int64_t);
  *val = (int64_t)*(char*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_CHAR_u8(Stream * str, Message *msg)

{
  uint8_t * val = new(uint8_t);
  *val = (uint8_t)*(char*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_CHAR_u16(Stream * str, Message *msg)

{
  uint16_t * val = new(uint16_t);
  *val = (uint16_t)*(char*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_CHAR_u32(Stream * str, Message *msg)

{
  uint32_t * val = new(uint32_t);
  *val = (uint32_t)*(char*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_CHAR_u64(Stream * str, Message *msg)

{
  uint64_t * val = new(uint64_t);
  *val = (uint64_t)*(char*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_CHAR_f32(Stream * str, Message *msg)

{
  float * val = new(float);
  *val = (float)*(char*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_CHAR_f64(Stream * str, Message *msg)

{
  double * val = new(double);
  *val = (double)*(char*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_CHAR_BOOL(Stream * str, Message *msg)

{
  bool * val = new(bool);
  *val = (bool)*(char*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

Message * _conv_CHAR_CHAR(Stream * str, Message *msg)

{
  char * val = new(char);
  *val = (char)*(char*)msg->payload;
  Message * out = new(Message);
  out -> type = str->output;
  out -> payload = val;
  out -> source = str;
  return out;
}

void * _get_CHAR(dtype out)
{
  switch(out) {
  case i8:
    return &_conv_CHAR_i8;
  case i16:
    return &_conv_CHAR_i16;
  case i32:
    return &_conv_CHAR_i32;
  case i64:
    return &_conv_CHAR_i64;
  case u8:
    return &_conv_CHAR_u8;
  case u16:
    return &_conv_CHAR_u16;
  case u32:
    return &_conv_CHAR_u32;
  case u64:
    return &_conv_CHAR_u64;
  case f32:
    return &_conv_CHAR_f32;
  case f64:
    return &_conv_CHAR_f64;
  case BOOL:
    return &_conv_CHAR_BOOL;
  case CHAR:
    return &_conv_CHAR_CHAR;
  default: return 0;
  }
}

Stream * convert(Stream *in, dtype out)
{
  void * fun;
  switch (in->output) {
  case i8:
    fun = _get_i8(out);
    break;
  case i16:
    fun = _get_i16(out);
    break;
  case i32:
    fun = _get_i32(out);
    break;
  case i64:
    fun = _get_i64(out);
    break;
  case u8:
    fun = _get_u8(out);
    break;
  case u16:
    fun = _get_u16(out);
    break;
  case u32:
    fun = _get_u32(out);
    break;
  case u64:
    fun = _get_u64(out);
    break;
  case f32:
    fun = _get_f32(out);
    break;
  case f64:
    fun = _get_f64(out);
    break;
  case BOOL:
    fun = _get_BOOL(out);
    break;
  case CHAR:
    fun = _get_CHAR(out);
    break;
  default:
    fun = 0;
    break;
  }
  Schedule s;
  s.type = ON_MESSAGE;
  Stream * str = add_stream(in->output, out, s, fun, 0);
  connect(in, str);
  return str;
}
