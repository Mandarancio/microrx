/**
 * \file utime.h
 * \brief Timing functionality header file
 * \author Martino Ferrari
 *
 * MicroRX timing utility.
 *
 */
#ifndef UTIME_H
#define UTIME_H
#include <stdint.h>

/**
 * \fn utime
 * \brief Returns the current time in micro-seconds (us)
 *
 * \return current time in us
 */
uint64_t        utime      ();
void            us_sleep   (uint64_t us);
#endif
