#include "basics.h"
#include <stdlib.h>
#include <stdint.h>


/***
 * AUTO GENERATED CODE
 * gen_map.py
 ***/

Message * _exec_i8_i8(Stream *str, Message *data)
{
  int8_t  val = *(int8_t*)data->payload;
  int8_t (*fn)(int8_t) = str->payload;
  int8_t *res = new(int8_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i8_u64(Stream *str, Message *data)
{
  int8_t  val = *(int8_t*)data->payload;
  uint64_t (*fn)(int8_t) = str->payload;
  uint64_t *res = new(uint64_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i8_i64(Stream *str, Message *data)
{
  int8_t  val = *(int8_t*)data->payload;
  int64_t (*fn)(int8_t) = str->payload;
  int64_t *res = new(int64_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i8_CHAR(Stream *str, Message *data)
{
  int8_t  val = *(int8_t*)data->payload;
  char (*fn)(int8_t) = str->payload;
  char *res = new(char);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i8_u8(Stream *str, Message *data)
{
  int8_t  val = *(int8_t*)data->payload;
  uint8_t (*fn)(int8_t) = str->payload;
  uint8_t *res = new(uint8_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i8_i32(Stream *str, Message *data)
{
  int8_t  val = *(int8_t*)data->payload;
  int32_t (*fn)(int8_t) = str->payload;
  int32_t *res = new(int32_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i8_i16(Stream *str, Message *data)
{
  int8_t  val = *(int8_t*)data->payload;
  int16_t (*fn)(int8_t) = str->payload;
  int16_t *res = new(int16_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i8_f32(Stream *str, Message *data)
{
  int8_t  val = *(int8_t*)data->payload;
  float (*fn)(int8_t) = str->payload;
  float *res = new(float);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i8_BOOL(Stream *str, Message *data)
{
  int8_t  val = *(int8_t*)data->payload;
  bool (*fn)(int8_t) = str->payload;
  bool *res = new(bool);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i8_u32(Stream *str, Message *data)
{
  int8_t  val = *(int8_t*)data->payload;
  uint32_t (*fn)(int8_t) = str->payload;
  uint32_t *res = new(uint32_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i8_f64(Stream *str, Message *data)
{
  int8_t  val = *(int8_t*)data->payload;
  double (*fn)(int8_t) = str->payload;
  double *res = new(double);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i8_u16(Stream *str, Message *data)
{
  int8_t  val = *(int8_t*)data->payload;
  uint16_t (*fn)(int8_t) = str->payload;
  uint16_t *res = new(uint16_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

void * _get_i8_func(dtype output)
{
  switch (output) {
    case i8: return &_exec_i8_i8;
    case u64: return &_exec_i8_u64;
    case i64: return &_exec_i8_i64;
    case CHAR: return &_exec_i8_CHAR;
    case u8: return &_exec_i8_u8;
    case i32: return &_exec_i8_i32;
    case i16: return &_exec_i8_i16;
    case f32: return &_exec_i8_f32;
    case BOOL: return &_exec_i8_BOOL;
    case u32: return &_exec_i8_u32;
    case f64: return &_exec_i8_f64;
    case u16: return &_exec_i8_u16;
    default: return 0;
  }
}

Message * _exec_u64_i8(Stream *str, Message *data)
{
  uint64_t  val = *(uint64_t*)data->payload;
  int8_t (*fn)(uint64_t) = str->payload;
  int8_t *res = new(int8_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u64_u64(Stream *str, Message *data)
{
  uint64_t  val = *(uint64_t*)data->payload;
  uint64_t (*fn)(uint64_t) = str->payload;
  uint64_t *res = new(uint64_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u64_i64(Stream *str, Message *data)
{
  uint64_t  val = *(uint64_t*)data->payload;
  int64_t (*fn)(uint64_t) = str->payload;
  int64_t *res = new(int64_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u64_CHAR(Stream *str, Message *data)
{
  uint64_t  val = *(uint64_t*)data->payload;
  char (*fn)(uint64_t) = str->payload;
  char *res = new(char);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u64_u8(Stream *str, Message *data)
{
  uint64_t  val = *(uint64_t*)data->payload;
  uint8_t (*fn)(uint64_t) = str->payload;
  uint8_t *res = new(uint8_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u64_i32(Stream *str, Message *data)
{
  uint64_t  val = *(uint64_t*)data->payload;
  int32_t (*fn)(uint64_t) = str->payload;
  int32_t *res = new(int32_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u64_i16(Stream *str, Message *data)
{
  uint64_t  val = *(uint64_t*)data->payload;
  int16_t (*fn)(uint64_t) = str->payload;
  int16_t *res = new(int16_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u64_f32(Stream *str, Message *data)
{
  uint64_t  val = *(uint64_t*)data->payload;
  float (*fn)(uint64_t) = str->payload;
  float *res = new(float);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u64_BOOL(Stream *str, Message *data)
{
  uint64_t  val = *(uint64_t*)data->payload;
  bool (*fn)(uint64_t) = str->payload;
  bool *res = new(bool);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u64_u32(Stream *str, Message *data)
{
  uint64_t  val = *(uint64_t*)data->payload;
  uint32_t (*fn)(uint64_t) = str->payload;
  uint32_t *res = new(uint32_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u64_f64(Stream *str, Message *data)
{
  uint64_t  val = *(uint64_t*)data->payload;
  double (*fn)(uint64_t) = str->payload;
  double *res = new(double);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u64_u16(Stream *str, Message *data)
{
  uint64_t  val = *(uint64_t*)data->payload;
  uint16_t (*fn)(uint64_t) = str->payload;
  uint16_t *res = new(uint16_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

void * _get_u64_func(dtype output)
{
  switch (output) {
    case i8: return &_exec_u64_i8;
    case u64: return &_exec_u64_u64;
    case i64: return &_exec_u64_i64;
    case CHAR: return &_exec_u64_CHAR;
    case u8: return &_exec_u64_u8;
    case i32: return &_exec_u64_i32;
    case i16: return &_exec_u64_i16;
    case f32: return &_exec_u64_f32;
    case BOOL: return &_exec_u64_BOOL;
    case u32: return &_exec_u64_u32;
    case f64: return &_exec_u64_f64;
    case u16: return &_exec_u64_u16;
    default: return 0;
  }
}

Message * _exec_i64_i8(Stream *str, Message *data)
{
  int64_t  val = *(int64_t*)data->payload;
  int8_t (*fn)(int64_t) = str->payload;
  int8_t *res = new(int8_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i64_u64(Stream *str, Message *data)
{
  int64_t  val = *(int64_t*)data->payload;
  uint64_t (*fn)(int64_t) = str->payload;
  uint64_t *res = new(uint64_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i64_i64(Stream *str, Message *data)
{
  int64_t  val = *(int64_t*)data->payload;
  int64_t (*fn)(int64_t) = str->payload;
  int64_t *res = new(int64_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i64_CHAR(Stream *str, Message *data)
{
  int64_t  val = *(int64_t*)data->payload;
  char (*fn)(int64_t) = str->payload;
  char *res = new(char);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i64_u8(Stream *str, Message *data)
{
  int64_t  val = *(int64_t*)data->payload;
  uint8_t (*fn)(int64_t) = str->payload;
  uint8_t *res = new(uint8_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i64_i32(Stream *str, Message *data)
{
  int64_t  val = *(int64_t*)data->payload;
  int32_t (*fn)(int64_t) = str->payload;
  int32_t *res = new(int32_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i64_i16(Stream *str, Message *data)
{
  int64_t  val = *(int64_t*)data->payload;
  int16_t (*fn)(int64_t) = str->payload;
  int16_t *res = new(int16_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i64_f32(Stream *str, Message *data)
{
  int64_t  val = *(int64_t*)data->payload;
  float (*fn)(int64_t) = str->payload;
  float *res = new(float);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i64_BOOL(Stream *str, Message *data)
{
  int64_t  val = *(int64_t*)data->payload;
  bool (*fn)(int64_t) = str->payload;
  bool *res = new(bool);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i64_u32(Stream *str, Message *data)
{
  int64_t  val = *(int64_t*)data->payload;
  uint32_t (*fn)(int64_t) = str->payload;
  uint32_t *res = new(uint32_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i64_f64(Stream *str, Message *data)
{
  int64_t  val = *(int64_t*)data->payload;
  double (*fn)(int64_t) = str->payload;
  double *res = new(double);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i64_u16(Stream *str, Message *data)
{
  int64_t  val = *(int64_t*)data->payload;
  uint16_t (*fn)(int64_t) = str->payload;
  uint16_t *res = new(uint16_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

void * _get_i64_func(dtype output)
{
  switch (output) {
    case i8: return &_exec_i64_i8;
    case u64: return &_exec_i64_u64;
    case i64: return &_exec_i64_i64;
    case CHAR: return &_exec_i64_CHAR;
    case u8: return &_exec_i64_u8;
    case i32: return &_exec_i64_i32;
    case i16: return &_exec_i64_i16;
    case f32: return &_exec_i64_f32;
    case BOOL: return &_exec_i64_BOOL;
    case u32: return &_exec_i64_u32;
    case f64: return &_exec_i64_f64;
    case u16: return &_exec_i64_u16;
    default: return 0;
  }
}

Message * _exec_CHAR_i8(Stream *str, Message *data)
{
  char  val = *(char*)data->payload;
  int8_t (*fn)(char) = str->payload;
  int8_t *res = new(int8_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_CHAR_u64(Stream *str, Message *data)
{
  char  val = *(char*)data->payload;
  uint64_t (*fn)(char) = str->payload;
  uint64_t *res = new(uint64_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_CHAR_i64(Stream *str, Message *data)
{
  char  val = *(char*)data->payload;
  int64_t (*fn)(char) = str->payload;
  int64_t *res = new(int64_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_CHAR_CHAR(Stream *str, Message *data)
{
  char  val = *(char*)data->payload;
  char (*fn)(char) = str->payload;
  char *res = new(char);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_CHAR_u8(Stream *str, Message *data)
{
  char  val = *(char*)data->payload;
  uint8_t (*fn)(char) = str->payload;
  uint8_t *res = new(uint8_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_CHAR_i32(Stream *str, Message *data)
{
  char  val = *(char*)data->payload;
  int32_t (*fn)(char) = str->payload;
  int32_t *res = new(int32_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_CHAR_i16(Stream *str, Message *data)
{
  char  val = *(char*)data->payload;
  int16_t (*fn)(char) = str->payload;
  int16_t *res = new(int16_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_CHAR_f32(Stream *str, Message *data)
{
  char  val = *(char*)data->payload;
  float (*fn)(char) = str->payload;
  float *res = new(float);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_CHAR_BOOL(Stream *str, Message *data)
{
  char  val = *(char*)data->payload;
  bool (*fn)(char) = str->payload;
  bool *res = new(bool);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_CHAR_u32(Stream *str, Message *data)
{
  char  val = *(char*)data->payload;
  uint32_t (*fn)(char) = str->payload;
  uint32_t *res = new(uint32_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_CHAR_f64(Stream *str, Message *data)
{
  char  val = *(char*)data->payload;
  double (*fn)(char) = str->payload;
  double *res = new(double);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_CHAR_u16(Stream *str, Message *data)
{
  char  val = *(char*)data->payload;
  uint16_t (*fn)(char) = str->payload;
  uint16_t *res = new(uint16_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

void * _get_CHAR_func(dtype output)
{
  switch (output) {
    case i8: return &_exec_CHAR_i8;
    case u64: return &_exec_CHAR_u64;
    case i64: return &_exec_CHAR_i64;
    case CHAR: return &_exec_CHAR_CHAR;
    case u8: return &_exec_CHAR_u8;
    case i32: return &_exec_CHAR_i32;
    case i16: return &_exec_CHAR_i16;
    case f32: return &_exec_CHAR_f32;
    case BOOL: return &_exec_CHAR_BOOL;
    case u32: return &_exec_CHAR_u32;
    case f64: return &_exec_CHAR_f64;
    case u16: return &_exec_CHAR_u16;
    default: return 0;
  }
}

Message * _exec_u8_i8(Stream *str, Message *data)
{
  uint8_t  val = *(uint8_t*)data->payload;
  int8_t (*fn)(uint8_t) = str->payload;
  int8_t *res = new(int8_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u8_u64(Stream *str, Message *data)
{
  uint8_t  val = *(uint8_t*)data->payload;
  uint64_t (*fn)(uint8_t) = str->payload;
  uint64_t *res = new(uint64_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u8_i64(Stream *str, Message *data)
{
  uint8_t  val = *(uint8_t*)data->payload;
  int64_t (*fn)(uint8_t) = str->payload;
  int64_t *res = new(int64_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u8_CHAR(Stream *str, Message *data)
{
  uint8_t  val = *(uint8_t*)data->payload;
  char (*fn)(uint8_t) = str->payload;
  char *res = new(char);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u8_u8(Stream *str, Message *data)
{
  uint8_t  val = *(uint8_t*)data->payload;
  uint8_t (*fn)(uint8_t) = str->payload;
  uint8_t *res = new(uint8_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u8_i32(Stream *str, Message *data)
{
  uint8_t  val = *(uint8_t*)data->payload;
  int32_t (*fn)(uint8_t) = str->payload;
  int32_t *res = new(int32_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u8_i16(Stream *str, Message *data)
{
  uint8_t  val = *(uint8_t*)data->payload;
  int16_t (*fn)(uint8_t) = str->payload;
  int16_t *res = new(int16_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u8_f32(Stream *str, Message *data)
{
  uint8_t  val = *(uint8_t*)data->payload;
  float (*fn)(uint8_t) = str->payload;
  float *res = new(float);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u8_BOOL(Stream *str, Message *data)
{
  uint8_t  val = *(uint8_t*)data->payload;
  bool (*fn)(uint8_t) = str->payload;
  bool *res = new(bool);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u8_u32(Stream *str, Message *data)
{
  uint8_t  val = *(uint8_t*)data->payload;
  uint32_t (*fn)(uint8_t) = str->payload;
  uint32_t *res = new(uint32_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u8_f64(Stream *str, Message *data)
{
  uint8_t  val = *(uint8_t*)data->payload;
  double (*fn)(uint8_t) = str->payload;
  double *res = new(double);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u8_u16(Stream *str, Message *data)
{
  uint8_t  val = *(uint8_t*)data->payload;
  uint16_t (*fn)(uint8_t) = str->payload;
  uint16_t *res = new(uint16_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

void * _get_u8_func(dtype output)
{
  switch (output) {
    case i8: return &_exec_u8_i8;
    case u64: return &_exec_u8_u64;
    case i64: return &_exec_u8_i64;
    case CHAR: return &_exec_u8_CHAR;
    case u8: return &_exec_u8_u8;
    case i32: return &_exec_u8_i32;
    case i16: return &_exec_u8_i16;
    case f32: return &_exec_u8_f32;
    case BOOL: return &_exec_u8_BOOL;
    case u32: return &_exec_u8_u32;
    case f64: return &_exec_u8_f64;
    case u16: return &_exec_u8_u16;
    default: return 0;
  }
}

Message * _exec_i32_i8(Stream *str, Message *data)
{
  int32_t  val = *(int32_t*)data->payload;
  int8_t (*fn)(int32_t) = str->payload;
  int8_t *res = new(int8_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i32_u64(Stream *str, Message *data)
{
  int32_t  val = *(int32_t*)data->payload;
  uint64_t (*fn)(int32_t) = str->payload;
  uint64_t *res = new(uint64_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i32_i64(Stream *str, Message *data)
{
  int32_t  val = *(int32_t*)data->payload;
  int64_t (*fn)(int32_t) = str->payload;
  int64_t *res = new(int64_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i32_CHAR(Stream *str, Message *data)
{
  int32_t  val = *(int32_t*)data->payload;
  char (*fn)(int32_t) = str->payload;
  char *res = new(char);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i32_u8(Stream *str, Message *data)
{
  int32_t  val = *(int32_t*)data->payload;
  uint8_t (*fn)(int32_t) = str->payload;
  uint8_t *res = new(uint8_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i32_i32(Stream *str, Message *data)
{
  int32_t  val = *(int32_t*)data->payload;
  int32_t (*fn)(int32_t) = str->payload;
  int32_t *res = new(int32_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i32_i16(Stream *str, Message *data)
{
  int32_t  val = *(int32_t*)data->payload;
  int16_t (*fn)(int32_t) = str->payload;
  int16_t *res = new(int16_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i32_f32(Stream *str, Message *data)
{
  int32_t  val = *(int32_t*)data->payload;
  float (*fn)(int32_t) = str->payload;
  float *res = new(float);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i32_BOOL(Stream *str, Message *data)
{
  int32_t  val = *(int32_t*)data->payload;
  bool (*fn)(int32_t) = str->payload;
  bool *res = new(bool);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i32_u32(Stream *str, Message *data)
{
  int32_t  val = *(int32_t*)data->payload;
  uint32_t (*fn)(int32_t) = str->payload;
  uint32_t *res = new(uint32_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i32_f64(Stream *str, Message *data)
{
  int32_t  val = *(int32_t*)data->payload;
  double (*fn)(int32_t) = str->payload;
  double *res = new(double);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i32_u16(Stream *str, Message *data)
{
  int32_t  val = *(int32_t*)data->payload;
  uint16_t (*fn)(int32_t) = str->payload;
  uint16_t *res = new(uint16_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

void * _get_i32_func(dtype output)
{
  switch (output) {
    case i8: return &_exec_i32_i8;
    case u64: return &_exec_i32_u64;
    case i64: return &_exec_i32_i64;
    case CHAR: return &_exec_i32_CHAR;
    case u8: return &_exec_i32_u8;
    case i32: return &_exec_i32_i32;
    case i16: return &_exec_i32_i16;
    case f32: return &_exec_i32_f32;
    case BOOL: return &_exec_i32_BOOL;
    case u32: return &_exec_i32_u32;
    case f64: return &_exec_i32_f64;
    case u16: return &_exec_i32_u16;
    default: return 0;
  }
}

Message * _exec_i16_i8(Stream *str, Message *data)
{
  int16_t  val = *(int16_t*)data->payload;
  int8_t (*fn)(int16_t) = str->payload;
  int8_t *res = new(int8_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i16_u64(Stream *str, Message *data)
{
  int16_t  val = *(int16_t*)data->payload;
  uint64_t (*fn)(int16_t) = str->payload;
  uint64_t *res = new(uint64_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i16_i64(Stream *str, Message *data)
{
  int16_t  val = *(int16_t*)data->payload;
  int64_t (*fn)(int16_t) = str->payload;
  int64_t *res = new(int64_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i16_CHAR(Stream *str, Message *data)
{
  int16_t  val = *(int16_t*)data->payload;
  char (*fn)(int16_t) = str->payload;
  char *res = new(char);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i16_u8(Stream *str, Message *data)
{
  int16_t  val = *(int16_t*)data->payload;
  uint8_t (*fn)(int16_t) = str->payload;
  uint8_t *res = new(uint8_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i16_i32(Stream *str, Message *data)
{
  int16_t  val = *(int16_t*)data->payload;
  int32_t (*fn)(int16_t) = str->payload;
  int32_t *res = new(int32_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i16_i16(Stream *str, Message *data)
{
  int16_t  val = *(int16_t*)data->payload;
  int16_t (*fn)(int16_t) = str->payload;
  int16_t *res = new(int16_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i16_f32(Stream *str, Message *data)
{
  int16_t  val = *(int16_t*)data->payload;
  float (*fn)(int16_t) = str->payload;
  float *res = new(float);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i16_BOOL(Stream *str, Message *data)
{
  int16_t  val = *(int16_t*)data->payload;
  bool (*fn)(int16_t) = str->payload;
  bool *res = new(bool);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i16_u32(Stream *str, Message *data)
{
  int16_t  val = *(int16_t*)data->payload;
  uint32_t (*fn)(int16_t) = str->payload;
  uint32_t *res = new(uint32_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i16_f64(Stream *str, Message *data)
{
  int16_t  val = *(int16_t*)data->payload;
  double (*fn)(int16_t) = str->payload;
  double *res = new(double);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_i16_u16(Stream *str, Message *data)
{
  int16_t  val = *(int16_t*)data->payload;
  uint16_t (*fn)(int16_t) = str->payload;
  uint16_t *res = new(uint16_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

void * _get_i16_func(dtype output)
{
  switch (output) {
    case i8: return &_exec_i16_i8;
    case u64: return &_exec_i16_u64;
    case i64: return &_exec_i16_i64;
    case CHAR: return &_exec_i16_CHAR;
    case u8: return &_exec_i16_u8;
    case i32: return &_exec_i16_i32;
    case i16: return &_exec_i16_i16;
    case f32: return &_exec_i16_f32;
    case BOOL: return &_exec_i16_BOOL;
    case u32: return &_exec_i16_u32;
    case f64: return &_exec_i16_f64;
    case u16: return &_exec_i16_u16;
    default: return 0;
  }
}

Message * _exec_f32_i8(Stream *str, Message *data)
{
  float  val = *(float*)data->payload;
  int8_t (*fn)(float) = str->payload;
  int8_t *res = new(int8_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_f32_u64(Stream *str, Message *data)
{
  float  val = *(float*)data->payload;
  uint64_t (*fn)(float) = str->payload;
  uint64_t *res = new(uint64_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_f32_i64(Stream *str, Message *data)
{
  float  val = *(float*)data->payload;
  int64_t (*fn)(float) = str->payload;
  int64_t *res = new(int64_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_f32_CHAR(Stream *str, Message *data)
{
  float  val = *(float*)data->payload;
  char (*fn)(float) = str->payload;
  char *res = new(char);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_f32_u8(Stream *str, Message *data)
{
  float  val = *(float*)data->payload;
  uint8_t (*fn)(float) = str->payload;
  uint8_t *res = new(uint8_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_f32_i32(Stream *str, Message *data)
{
  float  val = *(float*)data->payload;
  int32_t (*fn)(float) = str->payload;
  int32_t *res = new(int32_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_f32_i16(Stream *str, Message *data)
{
  float  val = *(float*)data->payload;
  int16_t (*fn)(float) = str->payload;
  int16_t *res = new(int16_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_f32_f32(Stream *str, Message *data)
{
  float  val = *(float*)data->payload;
  float (*fn)(float) = str->payload;
  float *res = new(float);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_f32_BOOL(Stream *str, Message *data)
{
  float  val = *(float*)data->payload;
  bool (*fn)(float) = str->payload;
  bool *res = new(bool);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_f32_u32(Stream *str, Message *data)
{
  float  val = *(float*)data->payload;
  uint32_t (*fn)(float) = str->payload;
  uint32_t *res = new(uint32_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_f32_f64(Stream *str, Message *data)
{
  float  val = *(float*)data->payload;
  double (*fn)(float) = str->payload;
  double *res = new(double);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_f32_u16(Stream *str, Message *data)
{
  float  val = *(float*)data->payload;
  uint16_t (*fn)(float) = str->payload;
  uint16_t *res = new(uint16_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

void * _get_f32_func(dtype output)
{
  switch (output) {
    case i8: return &_exec_f32_i8;
    case u64: return &_exec_f32_u64;
    case i64: return &_exec_f32_i64;
    case CHAR: return &_exec_f32_CHAR;
    case u8: return &_exec_f32_u8;
    case i32: return &_exec_f32_i32;
    case i16: return &_exec_f32_i16;
    case f32: return &_exec_f32_f32;
    case BOOL: return &_exec_f32_BOOL;
    case u32: return &_exec_f32_u32;
    case f64: return &_exec_f32_f64;
    case u16: return &_exec_f32_u16;
    default: return 0;
  }
}

Message * _exec_BOOL_i8(Stream *str, Message *data)
{
  bool  val = *(bool*)data->payload;
  int8_t (*fn)(bool) = str->payload;
  int8_t *res = new(int8_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_BOOL_u64(Stream *str, Message *data)
{
  bool  val = *(bool*)data->payload;
  uint64_t (*fn)(bool) = str->payload;
  uint64_t *res = new(uint64_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_BOOL_i64(Stream *str, Message *data)
{
  bool  val = *(bool*)data->payload;
  int64_t (*fn)(bool) = str->payload;
  int64_t *res = new(int64_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_BOOL_CHAR(Stream *str, Message *data)
{
  bool  val = *(bool*)data->payload;
  char (*fn)(bool) = str->payload;
  char *res = new(char);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_BOOL_u8(Stream *str, Message *data)
{
  bool  val = *(bool*)data->payload;
  uint8_t (*fn)(bool) = str->payload;
  uint8_t *res = new(uint8_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_BOOL_i32(Stream *str, Message *data)
{
  bool  val = *(bool*)data->payload;
  int32_t (*fn)(bool) = str->payload;
  int32_t *res = new(int32_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_BOOL_i16(Stream *str, Message *data)
{
  bool  val = *(bool*)data->payload;
  int16_t (*fn)(bool) = str->payload;
  int16_t *res = new(int16_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_BOOL_f32(Stream *str, Message *data)
{
  bool  val = *(bool*)data->payload;
  float (*fn)(bool) = str->payload;
  float *res = new(float);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_BOOL_BOOL(Stream *str, Message *data)
{
  bool  val = *(bool*)data->payload;
  bool (*fn)(bool) = str->payload;
  bool *res = new(bool);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_BOOL_u32(Stream *str, Message *data)
{
  bool  val = *(bool*)data->payload;
  uint32_t (*fn)(bool) = str->payload;
  uint32_t *res = new(uint32_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_BOOL_f64(Stream *str, Message *data)
{
  bool  val = *(bool*)data->payload;
  double (*fn)(bool) = str->payload;
  double *res = new(double);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_BOOL_u16(Stream *str, Message *data)
{
  bool  val = *(bool*)data->payload;
  uint16_t (*fn)(bool) = str->payload;
  uint16_t *res = new(uint16_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

void * _get_BOOL_func(dtype output)
{
  switch (output) {
    case i8: return &_exec_BOOL_i8;
    case u64: return &_exec_BOOL_u64;
    case i64: return &_exec_BOOL_i64;
    case CHAR: return &_exec_BOOL_CHAR;
    case u8: return &_exec_BOOL_u8;
    case i32: return &_exec_BOOL_i32;
    case i16: return &_exec_BOOL_i16;
    case f32: return &_exec_BOOL_f32;
    case BOOL: return &_exec_BOOL_BOOL;
    case u32: return &_exec_BOOL_u32;
    case f64: return &_exec_BOOL_f64;
    case u16: return &_exec_BOOL_u16;
    default: return 0;
  }
}

Message * _exec_u32_i8(Stream *str, Message *data)
{
  uint32_t  val = *(uint32_t*)data->payload;
  int8_t (*fn)(uint32_t) = str->payload;
  int8_t *res = new(int8_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u32_u64(Stream *str, Message *data)
{
  uint32_t  val = *(uint32_t*)data->payload;
  uint64_t (*fn)(uint32_t) = str->payload;
  uint64_t *res = new(uint64_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u32_i64(Stream *str, Message *data)
{
  uint32_t  val = *(uint32_t*)data->payload;
  int64_t (*fn)(uint32_t) = str->payload;
  int64_t *res = new(int64_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u32_CHAR(Stream *str, Message *data)
{
  uint32_t  val = *(uint32_t*)data->payload;
  char (*fn)(uint32_t) = str->payload;
  char *res = new(char);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u32_u8(Stream *str, Message *data)
{
  uint32_t  val = *(uint32_t*)data->payload;
  uint8_t (*fn)(uint32_t) = str->payload;
  uint8_t *res = new(uint8_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u32_i32(Stream *str, Message *data)
{
  uint32_t  val = *(uint32_t*)data->payload;
  int32_t (*fn)(uint32_t) = str->payload;
  int32_t *res = new(int32_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u32_i16(Stream *str, Message *data)
{
  uint32_t  val = *(uint32_t*)data->payload;
  int16_t (*fn)(uint32_t) = str->payload;
  int16_t *res = new(int16_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u32_f32(Stream *str, Message *data)
{
  uint32_t  val = *(uint32_t*)data->payload;
  float (*fn)(uint32_t) = str->payload;
  float *res = new(float);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u32_BOOL(Stream *str, Message *data)
{
  uint32_t  val = *(uint32_t*)data->payload;
  bool (*fn)(uint32_t) = str->payload;
  bool *res = new(bool);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u32_u32(Stream *str, Message *data)
{
  uint32_t  val = *(uint32_t*)data->payload;
  uint32_t (*fn)(uint32_t) = str->payload;
  uint32_t *res = new(uint32_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u32_f64(Stream *str, Message *data)
{
  uint32_t  val = *(uint32_t*)data->payload;
  double (*fn)(uint32_t) = str->payload;
  double *res = new(double);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u32_u16(Stream *str, Message *data)
{
  uint32_t  val = *(uint32_t*)data->payload;
  uint16_t (*fn)(uint32_t) = str->payload;
  uint16_t *res = new(uint16_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

void * _get_u32_func(dtype output)
{
  switch (output) {
    case i8: return &_exec_u32_i8;
    case u64: return &_exec_u32_u64;
    case i64: return &_exec_u32_i64;
    case CHAR: return &_exec_u32_CHAR;
    case u8: return &_exec_u32_u8;
    case i32: return &_exec_u32_i32;
    case i16: return &_exec_u32_i16;
    case f32: return &_exec_u32_f32;
    case BOOL: return &_exec_u32_BOOL;
    case u32: return &_exec_u32_u32;
    case f64: return &_exec_u32_f64;
    case u16: return &_exec_u32_u16;
    default: return 0;
  }
}

Message * _exec_f64_i8(Stream *str, Message *data)
{
  double  val = *(double*)data->payload;
  int8_t (*fn)(double) = str->payload;
  int8_t *res = new(int8_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_f64_u64(Stream *str, Message *data)
{
  double  val = *(double*)data->payload;
  uint64_t (*fn)(double) = str->payload;
  uint64_t *res = new(uint64_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_f64_i64(Stream *str, Message *data)
{
  double  val = *(double*)data->payload;
  int64_t (*fn)(double) = str->payload;
  int64_t *res = new(int64_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_f64_CHAR(Stream *str, Message *data)
{
  double  val = *(double*)data->payload;
  char (*fn)(double) = str->payload;
  char *res = new(char);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_f64_u8(Stream *str, Message *data)
{
  double  val = *(double*)data->payload;
  uint8_t (*fn)(double) = str->payload;
  uint8_t *res = new(uint8_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_f64_i32(Stream *str, Message *data)
{
  double  val = *(double*)data->payload;
  int32_t (*fn)(double) = str->payload;
  int32_t *res = new(int32_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_f64_i16(Stream *str, Message *data)
{
  double  val = *(double*)data->payload;
  int16_t (*fn)(double) = str->payload;
  int16_t *res = new(int16_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_f64_f32(Stream *str, Message *data)
{
  double  val = *(double*)data->payload;
  float (*fn)(double) = str->payload;
  float *res = new(float);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_f64_BOOL(Stream *str, Message *data)
{
  double  val = *(double*)data->payload;
  bool (*fn)(double) = str->payload;
  bool *res = new(bool);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_f64_u32(Stream *str, Message *data)
{
  double  val = *(double*)data->payload;
  uint32_t (*fn)(double) = str->payload;
  uint32_t *res = new(uint32_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_f64_f64(Stream *str, Message *data)
{
  double  val = *(double*)data->payload;
  double (*fn)(double) = str->payload;
  double *res = new(double);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_f64_u16(Stream *str, Message *data)
{
  double  val = *(double*)data->payload;
  uint16_t (*fn)(double) = str->payload;
  uint16_t *res = new(uint16_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

void * _get_f64_func(dtype output)
{
  switch (output) {
    case i8: return &_exec_f64_i8;
    case u64: return &_exec_f64_u64;
    case i64: return &_exec_f64_i64;
    case CHAR: return &_exec_f64_CHAR;
    case u8: return &_exec_f64_u8;
    case i32: return &_exec_f64_i32;
    case i16: return &_exec_f64_i16;
    case f32: return &_exec_f64_f32;
    case BOOL: return &_exec_f64_BOOL;
    case u32: return &_exec_f64_u32;
    case f64: return &_exec_f64_f64;
    case u16: return &_exec_f64_u16;
    default: return 0;
  }
}

Message * _exec_u16_i8(Stream *str, Message *data)
{
  uint16_t  val = *(uint16_t*)data->payload;
  int8_t (*fn)(uint16_t) = str->payload;
  int8_t *res = new(int8_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u16_u64(Stream *str, Message *data)
{
  uint16_t  val = *(uint16_t*)data->payload;
  uint64_t (*fn)(uint16_t) = str->payload;
  uint64_t *res = new(uint64_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u16_i64(Stream *str, Message *data)
{
  uint16_t  val = *(uint16_t*)data->payload;
  int64_t (*fn)(uint16_t) = str->payload;
  int64_t *res = new(int64_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u16_CHAR(Stream *str, Message *data)
{
  uint16_t  val = *(uint16_t*)data->payload;
  char (*fn)(uint16_t) = str->payload;
  char *res = new(char);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u16_u8(Stream *str, Message *data)
{
  uint16_t  val = *(uint16_t*)data->payload;
  uint8_t (*fn)(uint16_t) = str->payload;
  uint8_t *res = new(uint8_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u16_i32(Stream *str, Message *data)
{
  uint16_t  val = *(uint16_t*)data->payload;
  int32_t (*fn)(uint16_t) = str->payload;
  int32_t *res = new(int32_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u16_i16(Stream *str, Message *data)
{
  uint16_t  val = *(uint16_t*)data->payload;
  int16_t (*fn)(uint16_t) = str->payload;
  int16_t *res = new(int16_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u16_f32(Stream *str, Message *data)
{
  uint16_t  val = *(uint16_t*)data->payload;
  float (*fn)(uint16_t) = str->payload;
  float *res = new(float);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u16_BOOL(Stream *str, Message *data)
{
  uint16_t  val = *(uint16_t*)data->payload;
  bool (*fn)(uint16_t) = str->payload;
  bool *res = new(bool);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u16_u32(Stream *str, Message *data)
{
  uint16_t  val = *(uint16_t*)data->payload;
  uint32_t (*fn)(uint16_t) = str->payload;
  uint32_t *res = new(uint32_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u16_f64(Stream *str, Message *data)
{
  uint16_t  val = *(uint16_t*)data->payload;
  double (*fn)(uint16_t) = str->payload;
  double *res = new(double);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

Message * _exec_u16_u16(Stream *str, Message *data)
{
  uint16_t  val = *(uint16_t*)data->payload;
  uint16_t (*fn)(uint16_t) = str->payload;
  uint16_t *res = new(uint16_t);
  *res = fn(val);
  Message * msg = new(Message);
  msg->payload = res;
  msg->source = str;
  msg->type = str->output;

  return msg;
}

void * _get_u16_func(dtype output)
{
  switch (output) {
    case i8: return &_exec_u16_i8;
    case u64: return &_exec_u16_u64;
    case i64: return &_exec_u16_i64;
    case CHAR: return &_exec_u16_CHAR;
    case u8: return &_exec_u16_u8;
    case i32: return &_exec_u16_i32;
    case i16: return &_exec_u16_i16;
    case f32: return &_exec_u16_f32;
    case BOOL: return &_exec_u16_BOOL;
    case u32: return &_exec_u16_u32;
    case f64: return &_exec_u16_f64;
    case u16: return &_exec_u16_u16;
    default: return 0;
  }
}


Stream * map        (Stream *  str,
                     void   *  fun,
                     dtype     type)
{
  void * func = 0;
  switch (str->output) {
  case i8:
    func = _get_i8_func(type);
    break;
  case u64:
    func = _get_u64_func(type);
    break;
  case i64:
    func = _get_i64_func(type);
    break;
  case CHAR:
    func = _get_CHAR_func(type);
    break;
  case u8:
    func = _get_u8_func(type);
    break;
  case i32:
    func = _get_i32_func(type);
    break;
  case i16:
    func = _get_i16_func(type);
    break;
  case f32:
    func = _get_f32_func(type);
    break;
  case BOOL:
    func = _get_BOOL_func(type);
    break;
  case u32:
    func = _get_u32_func(type);
    break;
  case f64:
    func = _get_f64_func(type);
    break;
  case u16:
    func = _get_u16_func(type);
    break;
  default:
    break;
  }
  Schedule s;
  s.type = ON_MESSAGE;
  Stream *target = add_stream(str->output, type, s, func, fun);
  connect(str, target);
  return target;
}


Message * _filter_i8_type(Stream * str, Message * msg)
{
  int8_t* val = msg->payload;
  bool (*fn)(int8_t) = str->payload;
  if (fn(*val)) {
    Message * out = new(Message);
    out -> payload = val;
    out -> type = str->output;
    out -> source = str;
    return out;
  }
  return 0;
}

Message * _filter_u64_type(Stream * str, Message * msg)
{
  uint64_t* val = msg->payload;
  bool (*fn)(uint64_t) = str->payload;
  if (fn(*val)) {
    Message * out = new(Message);
    out -> payload = val;
    out -> type = str->output;
    out -> source = str;
    return out;
  }
  return 0;
}

Message * _filter_i64_type(Stream * str, Message * msg)
{
  int64_t* val = msg->payload;
  bool (*fn)(int64_t) = str->payload;
  if (fn(*val)) {
    Message * out = new(Message);
    out -> payload = val;
    out -> type = str->output;
    out -> source = str;
    return out;
  }
  return 0;
}

Message * _filter_CHAR_type(Stream * str, Message * msg)
{
  char* val = msg->payload;
  bool (*fn)(char) = str->payload;
  if (fn(*val)) {
    Message * out = new(Message);
    out -> payload = val;
    out -> type = str->output;
    out -> source = str;
    return out;
  }
  return 0;
}

Message * _filter_u8_type(Stream * str, Message * msg)
{
  uint8_t* val = msg->payload;
  bool (*fn)(uint8_t) = str->payload;
  if (fn(*val)) {
    Message * out = new(Message);
    out -> payload = val;
    out -> type = str->output;
    out -> source = str;
    return out;
  }
  return 0;
}

Message * _filter_i32_type(Stream * str, Message * msg)
{
  int32_t* val = msg->payload;
  bool (*fn)(int32_t) = str->payload;
  if (fn(*val)) {
    Message * out = new(Message);
    out -> payload = val;
    out -> type = str->output;
    out -> source = str;
    return out;
  }
  return 0;
}

Message * _filter_i16_type(Stream * str, Message * msg)
{
  int16_t* val = msg->payload;
  bool (*fn)(int16_t) = str->payload;
  if (fn(*val)) {
    Message * out = new(Message);
    out -> payload = val;
    out -> type = str->output;
    out -> source = str;
    return out;
  }
  return 0;
}

Message * _filter_f32_type(Stream * str, Message * msg)
{
  float* val = msg->payload;
  bool (*fn)(float) = str->payload;
  if (fn(*val)) {
    Message * out = new(Message);
    out -> payload = val;
    out -> type = str->output;
    out -> source = str;
    return out;
  }
  return 0;
}

Message * _filter_BOOL_type(Stream * str, Message * msg)
{
  bool* val = msg->payload;
  bool (*fn)(bool) = str->payload;
  if (fn(*val)) {
    Message * out = new(Message);
    out -> payload = val;
    out -> type = str->output;
    out -> source = str;
    return out;
  }
  return 0;
}

Message * _filter_u32_type(Stream * str, Message * msg)
{
  uint32_t* val = msg->payload;
  bool (*fn)(uint32_t) = str->payload;
  if (fn(*val)) {
    Message * out = new(Message);
    out -> payload = val;
    out -> type = str->output;
    out -> source = str;
    return out;
  }
  return 0;
}

Message * _filter_f64_type(Stream * str, Message * msg)
{
  double* val = msg->payload;
  bool (*fn)(double) = str->payload;
  if (fn(*val)) {
    Message * out = new(Message);
    out -> payload = val;
    out -> type = str->output;
    out -> source = str;
    return out;
  }
  return 0;
}

Message * _filter_u16_type(Stream * str, Message * msg)
{
  uint16_t* val = msg->payload;
  bool (*fn)(uint16_t) = str->payload;
  if (fn(*val)) {
    Message * out = new(Message);
    out -> payload = val;
    out -> type = str->output;
    out -> source = str;
    return out;
  }
  return 0;
}


Stream * filter     (Stream *  str,
                     void          *  fun)
{
  void * func = 0;
  switch (str->output) {
  case i8:
    func = &_filter_i8_type;
    break;
  case u64:
    func = &_filter_u64_type;
    break;
  case i64:
    func = &_filter_i64_type;
    break;
  case CHAR:
    func = &_filter_CHAR_type;
    break;
  case u8:
    func = &_filter_u8_type;
    break;
  case i32:
    func = &_filter_i32_type;
    break;
  case i16:
    func = &_filter_i16_type;
    break;
  case f32:
    func = &_filter_f32_type;
    break;
  case BOOL:
    func = &_filter_BOOL_type;
    break;
  case u32:
    func = &_filter_u32_type;
    break;
  case f64:
    func = &_filter_f64_type;
    break;
  case u16:
    func = &_filter_u16_type;
    break;
  default:
    break;
  }
  Schedule s;
  s.type = ON_MESSAGE;
  Stream *target = add_stream(str->output, str->output, s, func, fun);
  connect(str, target);
  return target;
}

