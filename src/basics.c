#include "basics.h"
#include "utime.h"
#include <stdlib.h>
#include <stdio.h>

Schedule on_message()
{
  Schedule s;
  s.type = ON_MESSAGE;
  return s;
}

Schedule on_timer(time us)
{
  Schedule s;
  s.previous_call = 0;
  s.interval = us;
  s.type = ON_TIMER;
  return s;
}

Message * _exec_timer(Stream  * self,
                      Message * input)
{
  Message * output = new(Message);
  output->source = self;
  output->payload = 0;
  output->type    = VOID;
  return output;
}

Stream * timer (time us)
{
  return add_stream (NONE, VOID, on_timer(us), &_exec_timer, 0);
}

Stream * tick (Stream *  str)
{
  Stream * target = add_stream (str->output, VOID, on_message(), &_exec_timer, 0);
  connect(str, target);
  return target;
}

Message * _payload(Stream   * self,
                   Message  * input)
{
  Message * output = new(Message);
  output->payload = self->payload;
  output->type = self->output;
  output->source = self;
  return output;
}


Stream * constant   (Stream *  str,
                     void   *  constant,
                     dtype     type)
{
  if (str->output != VOID)
    return 0;
  Stream *target = add_stream (VOID, type, on_message (), &_payload, constant);
  connect(str, target);
  return target;
}

struct _merge_payload {
  uint32_t id_l;
  uint32_t id_r;
  Pair * result;
  bool left;
  bool right;
};

Message * _exec_merge(Stream  * self,
                      Message * input)
{
  struct _merge_payload * payload = self->payload;
  if (input->source->id == payload->id_l){
    payload -> left = true;
    payload -> result -> left = input -> payload;
    payload -> result -> l_type = input -> type;
  } else if (input -> source -> id == payload -> id_r) {
    payload -> right = true;
    payload -> result -> right = input -> payload;
    payload -> result -> r_type = input -> type;
  }
  if (payload -> left && payload -> right) {
    void * res = payload -> result;
    payload -> result = new(Pair);
    payload -> left = false;
    payload -> right = false;
    Message * msg = new(Message);
    msg->payload = res;
    msg->type = PAIR;
    msg->source = self;
    return msg;
  }
  return 0;
}

Stream * merge (Stream *  str0,
                Stream *  str1)
{
  struct _merge_payload * payload = new(struct _merge_payload);
  payload->left = false;
  payload->right = false;
  payload->id_l = str0->id;
  payload->id_r = str1->id;
  Stream * str = add_stream (ANY, PAIR, on_message (), &_exec_merge, payload);
  connect(str0, str);
  connect(str1, str);
  return str;
}

struct _buffer_n {
  List * lst;
  uint32_t n;
  uint32_t count;
};

struct _buffer_t {
  List *lst;
};

Message * _exc_buffer_n(Stream * self,
                        Message *msg)
{
  struct _buffer_n * bff = self->payload;
  bff->lst = push_back(bff->lst, msg->payload, msg->type);\
  bff->count ++;
  if (bff->count >= bff->n) {
    Message * res = new(Message);
    res->payload = bff->lst;
    res->type = LIST;
    res->source = self;
    bff->lst = 0;
    bff->count = 0;
    return res;
  }
  return 0;
}


Message * _exc_buffer_t(Stream  *self,
                        Message *msg)
{
  struct _buffer_t * bff = self->payload;
  bff->lst = push_back(bff->lst, msg->payload, msg->type);
  return 0;
}

Message * _trig_buffer_t(Stream * self,
                         Message * msg)
{
  struct _buffer_t * bff = self->payload;
  Message * res = new(Message);
  res->payload = bff->lst;
  res->source = self;
  res->type = LIST;
  bff->lst = 0;
  return res;
}

Stream * buffer_n(Stream   * in,
                  uint32_t   n)
{
  struct _buffer_n * bff = new(struct _buffer_n);
  bff->lst = 0;
  bff->n = n;
  bff->count = 0;
  Stream * out = add_stream(in->output, LIST, on_message (), &_exc_buffer_n, bff);
  connect(in, out);
  return out;
}

Stream * buffer_t(Stream   * in,
                  uint32_t   us)
{
  struct _buffer_t * bff = new(struct _buffer_t);
  bff->lst = 0;
  Stream * internal = add_stream(in->output, LIST, on_message (), &_exc_buffer_t, bff);
  connect(in, internal);
  return add_stream (NONE, LIST, on_timer (us), &_trig_buffer_t, bff);
}

struct _counter_t {
  uint32_t count;
};

struct _counter_n {
  uint32_t count;
  uint32_t max;
};


Message * _count_n(Stream * self,
                   Message * input)
{
  struct _counter_n * counter = self->payload;
  counter->count++;
  if (counter->count >= counter->max) {
    Message * output = new(Message);
    uint32_t * val = new(uint32_t);
    *val = counter->count;
    output->payload = val;
    output->source = self;
    output->type = self->output;
    counter->count = 0;
    return output;
  }
  return 0;
}

Message * _count_t(Stream* str,
                   Message* msg)
{
  struct _counter_t * counter = str->payload;
  counter->count++;
  return 0;
}

Message * _trig_count_t(Stream  *self,
                        Message *in)
{
  struct _counter_t * bff = self->payload;
  uint32_t * c = new(uint32_t);
  *c = bff->count;
  bff->count = 0;
  Message * msg = new(Message);
  msg->payload = c;
  msg->type = self->output;
  msg->source = self;
  return msg;
}

Stream * count_n(Stream   * in,
                 uint32_t   n)
{
  struct _counter_n * counter = new(struct _counter_n);
  counter->count = 0;
  counter->max = n;
  Stream * out = add_stream(in->output, u32, on_message (), &_count_n, counter);
  connect(in, out);
  return out;
}


Stream * count_t(Stream   * in,
                 uint32_t   us)
{
  struct _counter_t * counter = new(struct _counter_t);
  counter->count = 0;
  Stream * out = add_stream(in->output, u32, on_message(), &_count_t, counter);
  connect(in, out);
  return add_stream (in->output, u32, on_timer(us), &_trig_count_t, counter);
}
