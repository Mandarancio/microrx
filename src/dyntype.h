#ifndef DTYPE_H
#define DTYPE_H
#include <stdint.h>

typedef enum _bool_ bool;
typedef enum _dtype_ dtype;

typedef struct _list_ List;
typedef struct _pair_ Pair;
typedef struct _dynarray_ DynArray;

enum _bool_ {
  false = 0,
  true = 1
};

enum _dtype_ {
  i8, i16, i32, i64, // signed integer
  u8, u16, u32, u64, // unsigned integer
  f32, f64, // floating point number
  BOOL, // boolean
  CHAR, // char
  LIST, // list
  PAIR, // pair (e.g. for merging streams)
  VOID, // void signals
  ANY,  // catch any type
  ERROR,// ERROR Type
  NONE  // catch no type
};


/**
 * \struct pair
 * \param left Left value of the pair.
 * \param right Right value of the pair
 * \param l_type dtype of left value
 * \param r_right dtype of right value
 */
struct _pair_ {
  void *      left;
  void *      right;
  dtype  l_type;
  dtype  r_type;
};

struct _list_{
  dtype type;
  void * value;
  List * next;
};

struct _dynarray_ {
  dtype    type;
  void *   array;
  uint32_t size;
};

List * push_back(List * head,
                 void * value,
                 dtype  type);

DynArray * insert(DynArray * self,
                  void     * value);

#endif
