#include "basics.h"
#include <stdio.h>
#include <stdlib.h>


/***
 * AUTO GENERATED CODE
 * gen_accumulate.py
 ***/

Message * _acc_i64(Stream *str, Message *msg)
{
  int64_t* acc = str->payload;
  int64_t* val = msg->payload;
  *acc += *val;
  Message * out = malloc(sizeof(Message));
  out->payload = acc;
  out->source = str;
  out->type = str->output;
  return out;
}

Message * _acc_i8(Stream *str, Message *msg)
{
  int8_t* acc = str->payload;
  int8_t* val = msg->payload;
  *acc += *val;
  Message * out = malloc(sizeof(Message));
  out->payload = acc;
  out->source = str;
  out->type = str->output;
  return out;
}

Message * _acc_f32(Stream *str, Message *msg)
{
  float* acc = str->payload;
  float* val = msg->payload;
  *acc += *val;
  Message * out = malloc(sizeof(Message));
  out->payload = acc;
  out->source = str;
  out->type = str->output;
  return out;
}

Message * _acc_f64(Stream *str, Message *msg)
{
  double* acc = str->payload;
  double* val = msg->payload;
  *acc += *val;
  Message * out = malloc(sizeof(Message));
  out->payload = acc;
  out->source = str;
  out->type = str->output;
  return out;
}

Message * _acc_u8(Stream *str, Message *msg)
{
  uint8_t* acc = str->payload;
  uint8_t* val = msg->payload;
  *acc += *val;
  Message * out = malloc(sizeof(Message));
  out->payload = acc;
  out->source = str;
  out->type = str->output;
  return out;
}

Message * _acc_i32(Stream *str, Message *msg)
{
  int32_t* acc = str->payload;
  int32_t* val = msg->payload;
  *acc += *val;
  Message * out = malloc(sizeof(Message));
  out->payload = acc;
  out->source = str;
  out->type = str->output;
  return out;
}

Message * _acc_i16(Stream *str, Message *msg)
{
  int16_t* acc = str->payload;
  int16_t* val = msg->payload;
  *acc += *val;
  Message * out = malloc(sizeof(Message));
  out->payload = acc;
  out->source = str;
  out->type = str->output;
  return out;
}

Message * _acc_u64(Stream *str, Message *msg)
{
  uint64_t* acc = str->payload;
  uint64_t* val = msg->payload;
  *acc += *val;
  Message * out = malloc(sizeof(Message));
  out->payload = acc;
  out->source = str;
  out->type = str->output;
  return out;
}

Message * _acc_u32(Stream *str, Message *msg)
{
  uint32_t* acc = str->payload;
  uint32_t* val = msg->payload;
  *acc += *val;
  Message * out = malloc(sizeof(Message));
  out->payload = acc;
  out->source = str;
  out->type = str->output;
  return out;
}

Message * _acc_u16(Stream *str, Message *msg)
{
  uint16_t* acc = str->payload;
  uint16_t* val = msg->payload;
  *acc += *val;
  Message * out = malloc(sizeof(Message));
  out->payload = acc;
  out->source = str;
  out->type = str->output;
  return out;
}

Stream * accumulate(Stream *str)
{
  Stream *target;
  Schedule s;
  s.type = ON_MESSAGE;
  void * payload;
  switch (str->output) {
  case i64: 
    payload = malloc(sizeof(int64_t));
    *(int64_t*)payload = 0;
    target = add_stream(str->output, str->output, s, &_acc_i64, payload);
    break;
  case i8: 
    payload = malloc(sizeof(int8_t));
    *(int8_t*)payload = 0;
    target = add_stream(str->output, str->output, s, &_acc_i8, payload);
    break;
  case f32: 
    payload = malloc(sizeof(float));
    *(float*)payload = 0;
    target = add_stream(str->output, str->output, s, &_acc_f32, payload);
    break;
  case f64: 
    payload = malloc(sizeof(double));
    *(double*)payload = 0;
    target = add_stream(str->output, str->output, s, &_acc_f64, payload);
    break;
  case u8: 
    payload = malloc(sizeof(uint8_t));
    *(uint8_t*)payload = 0;
    target = add_stream(str->output, str->output, s, &_acc_u8, payload);
    break;
  case i32: 
    payload = malloc(sizeof(int32_t));
    *(int32_t*)payload = 0;
    target = add_stream(str->output, str->output, s, &_acc_i32, payload);
    break;
  case i16: 
    payload = malloc(sizeof(int16_t));
    *(int16_t*)payload = 0;
    target = add_stream(str->output, str->output, s, &_acc_i16, payload);
    break;
  case u64: 
    payload = malloc(sizeof(uint64_t));
    *(uint64_t*)payload = 0;
    target = add_stream(str->output, str->output, s, &_acc_u64, payload);
    break;
  case u32: 
    payload = malloc(sizeof(uint32_t));
    *(uint32_t*)payload = 0;
    target = add_stream(str->output, str->output, s, &_acc_u32, payload);
    break;
  case u16: 
    payload = malloc(sizeof(uint16_t));
    *(uint16_t*)payload = 0;
    target = add_stream(str->output, str->output, s, &_acc_u16, payload);
    break;
  default:
    target = 0;
    break;
  }
  if (target)
    connect(str, target);
  return target;
}
