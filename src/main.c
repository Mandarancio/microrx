#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include "microrx.h"
#include "basics.h"

#define ms *1000
#define s *1000 ms

bool check(uint32_t f) {
  return (int)f % 3 == 0;
}

int main() {
  init();
  // example of two counter with different timing 300ms and 100ms.
  uint32_t one = 1;
  Stream * tmr_3ms = timer(300 ms);
  Stream * acc = accumulate(constant(tmr_3ms, &one, u32));
  printstr(map(convert(acc, f64), &cos, f64));
  printstr(filter(acc, &check));
  printstr(count_t(tmr_3ms, 1 s));
  return loop();
}
