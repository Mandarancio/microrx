#include "utime.h"
#if TRGT_LINUX
#include <sys/time.h>
#include <unistd.h>

uint64_t utime() {
  /***
   * Retrives the time in micro seconds (us).
   * Return: time in us.
   ***/
  struct timeval tv;
  gettimeofday(&tv, 0);
  return tv.tv_sec*(uint64_t)1000000+tv.tv_usec;
}

void us_sleep(uint64_t us)
{
  usleep(us);
}

#elif TRGT_AVR
#include <avr/delay.h>
#define clockCyclesPerMicrosecond  F_CPU / 1000000L

uint64_t utime() {
  /***
   * Retrives the time in micro seconds (us).
   * Return: time in us.
   ***/
  // TODO implement that.
  uint64_t m;
  uint8_t oldSREG = SREG, t;

    cli();
    m = timer0_overflow_count;
#if defined(TCNT0)
    t = TCNT0;
#elif defined(TCNT0L)
    t = TCNT0L;
#else
    #error TIMER 0 not defined
#endif


#ifdef TIFR0
    if ((TIFR0 & _BV(TOV0)) && (t & 255))
        m++;
#else
    if ((TIFR & _BV(TOV0)) && (t & 255))
        m++;
#endif

    SREG = oldSREG;

    return ((m << 8) + t) * (64 / clockCyclesPerMicrosecond());
}

void us_sleep`(uint64_t us) {
  _delay_us (us);
}
#endif

