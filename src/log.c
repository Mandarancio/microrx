#include "log.h"
#include <stdlib.h>
#include <stdio.h>


void _log(char * msg,
          ...)
{
  va_list args;
  printf(msg, args);
  printf("\n");
}

void _warning(char *msg,
              ...)
{
  va_list args;
  printf("Warning: ");
  printf(msg, args);
  printf("\n");
}

void _error(char *msg,
              ...)
{
  va_list args;
  printf("Error: ");
  printf(msg, args);
  printf("\n");
}
struct logger * stdio_logger()
{
  struct logger * logr = (struct logger *)malloc(sizeof(struct logger));
  logr->log = &_log;
  logr->warning = &_warning ;
  logr->error = &_error;
  return logr;
}
