#include "microrx.h"
#include "utime.h"
#include <stdlib.h>

#define forever while (true)

                  typedef struct _connection_ Connection;
typedef uint32_t id;

struct Manager {
  Stream     **streams;
  uint32_t     streams_len;
  Message    **messages;
  Connection **connections;
} manager;


struct _connection_ {
  uint32_t source;
  uint32_t target;
  Connection * next;
};

void init() {
  manager.streams_len = 0;
  manager.streams = 0;
  manager.messages = 0;
  manager.connections = 0;
}

Message * consume_msg(uint32_t id)
{
  Message * msg = manager.messages[id];
  if (msg)
    manager.messages[id] = msg->next;
  return msg;
}

Message * copy(Message * msg)
{
  Message * m = new(Message);
  m->payload = msg->payload; // TODO Copy Payload
  m->source = msg->source;
  m->timestamp = msg->timestamp;
  m->type = msg->type;
  m->next = 0;
  return m;
}

void dispatch_message(Message * res)
{
  if (res->type == ERROR) {
    // TODO LOG ERROR
    free(res);
    return;
  }
  Connection * cnn = manager.connections[res->source->id];
  while (cnn){
    Message * msg = copy(res);
    Message * el = manager.messages[cnn->target];
    if (el) {
      while (el->next) {
        el = el->next;
      }
      el->next = msg;
    } else {
      manager.messages[cnn->target] = msg;
    }
    cnn = cnn->next;
  }
  free(res);
}

time compute_next_schedule(time now)
{
  time next = UINT32_MAX;
  id i;
  for (i = 0; i < manager.streams_len; i++)
  {
    if (manager.messages[i])
      return 0;
    Schedule s = manager.streams[i]->schedule;
    if (s.type == ON_TIMER && s.interval - (now - s.previous_call) < next)
      next = s.interval - (now - s.previous_call) < next;
  }
  return next;
}

int loop()
{
  forever
  {
    time now = utime();

    id i;
    for (i = 0; i<manager.streams_len; i++)
    {
      Stream * current = manager.streams[i];

      if ((current->schedule.type == ON_MESSAGE && manager.messages[i]) ||
          (current->schedule.type == ON_TIMER &&
           now - current->schedule.previous_call >= current->schedule.interval))
      {
        Message * input = consume_msg (i);
        Message * output = current->callback(current, input);
        if (output)
        {
          output->timestamp = now;
          dispatch_message (output);
          current->schedule.previous_call = now;
        }
      }
    }
    time us = compute_next_schedule(now); // TODO implement it on the run.
    us_sleep(us);
  }
  return 0;
}

Stream * __new__(uint32_t id,
                  dtype    input,
                  dtype    output,
                  Schedule schedule,
                  Message*(*callback) (Stream  * self,
                                       Message * input),
                  void   * payload)
{
  Stream * str = new(Stream);
  str->id = id;
  str->input = input;
  str->output = output;
  str->schedule = schedule;
  str->callback = callback;
  str->payload = payload;
  return str;
}

Stream * add_stream(dtype    input,
                    dtype    output,
                    Schedule schedule,
                    Message*(*callback) (Stream  * self,
                                         Message * input),
                    void   * payload)
{
  uint32_t id = manager.streams_len;
  Stream * str = __new__(id,
                         input,
                         output,
                         schedule,
                         callback,
                         payload);

  if (manager.streams_len) {
    uint32_t n = manager.streams_len + 1;
    manager.streams = resize(manager.streams,Stream*, n);
    manager.messages = resize(manager.messages, Message*, n);
    manager.connections = resize(manager.connections,Connection*, n);
  } else {
    manager.streams = new(Stream*);
    manager.messages = new(Message*);
    manager.connections = new(Connection*);
  }
  manager.streams_len ++;
  manager.streams[id] = str;
  manager.connections[id] = 0;
  manager.messages[id] = 0;
  return str;
}


bool connect    (Stream *  source,
                 Stream *  target)
{
  if (target->input == ANY || target->input == source->output){
    Connection * c = new(Connection);
    c->source = source->id;
    c->target = target->id;
    c->next = manager.connections[c->source];
    manager.connections[c->source] = c;
    return true;
  }
  return false;
}

#if TEST == 1
int main(){
  init();
  if (manager.streams_len == 0)
    return 0;
  return 1;
}
#endif
