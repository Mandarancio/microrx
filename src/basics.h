/**
 * \file basics.h
 * \brief Basic blocks and function for MicroRX
 * \author Martino Ferrari
 *
 * MicroRX basic operands and funcrions
 *
 */
#ifndef BASICS_H
#define BASICS_H
#include "microrx.h"


Stream * timer      (time      us);       // Generator
Stream * tick       (Stream *  str);      // Convert any stream to a timer stream
Stream * constant   (Stream *  str,       // emit constant value
                     void   *  constant,
                     dtype     type);

Stream * accumulate (Stream *  str);      // Accumulate number

Stream * merge      (Stream *  str0,
                     Stream *  str1);

Stream * printstr   (Stream *  str);      // Print value

/*!
 * \fn map
 * \brief Create a new soft stream (SW_STREAM) by mapping a regular function to an existing stream
 *
 * \param str input stream
 * \param fun function to map
 * \param return_type output data type
 *
 * \return a stream representing the mapping of the function fun to the steeam str
 */
Stream * map        (Stream *  str,
                     void   *  fun,
                     dtype     return_type);

/*!
 * \fn filter
 * \brief Create a new check stream (CH_STREAM) using a filter function
 *
 * \param str input stream
 * \param fun pointer to a boolean function with input type equal to str output type
 *
 * \return a check stream of same time of str but emiting a new value only when fun is soddisfied
 */
Stream * filter     (Stream *  str,
                      void  *  fun); // bool fun(str->output)

Stream * convert    (Stream *  in,
                     dtype     out);

Stream * buffer_n   (Stream *  in,
                     uint32_t  n);
Stream * buffer_t   (Stream *  in,
                     uint32_t  us);

Stream * count_n    (Stream *  in,
                     uint32_t  n);
Stream * count_t    (Stream *  in,
                     uint32_t  us);

#endif
