enums = { 'i8'  : 'int8_t',
          'i16' : 'int16_t',
          'i32' : 'int32_t',
          'i64' : 'int64_t',
          'u8'  : 'uint8_t',
          'u16' : 'uint16_t',
          'u32' : 'uint32_t',
          'u64' : 'uint64_t',
          'f32' : 'float',
          'f64' : 'double',
          'BOOL': 'bool',
          'CHAR': 'char',
}


print('#include "basics.h"\n#include <stdlib.h>\n\n')
print('/***')
print(' * AUTO GENERATED CODE')
print(' * gen_convert.py')
print(' ***/\n')

main_func = """Stream * convert(Stream *in, dtype out)
{
  void * fun;
  switch (in->output) {
"""

for enum in enums:
    main_func += "  case "+enum+":\n"
    main_func += "    fun = _get_"+enum+"(out);\n"
    main_func += "    break;\n"
    get_func = "void * _get_"+enum+"(dtype out)\n{\n"
    get_func += "  switch(out) {\n"
    for out in enums:
        print(f"Message * _conv_{enum}_{out}(Stream * str, Message *msg)\n")
        print("{")
        print(f"  {enums[out]} * val = new({enums[out]});")
        print(f"  *val = ({enums[out]})*({enums[enum]}*)msg->payload;")
        print(f"  Message * out = new(Message);")
        print(f"  out -> type = str->output;")
        print(f"  out -> payload = val;")
        print(f"  out -> source = str;")
        print(f'  return out;')
        print("}\n")

        get_func += f"  case {out}:\n"
        get_func += f"    return &_conv_{enum}_{out};\n"
    get_func += "  default: return 0;\n  }\n}\n"
    print(get_func)
main_func +="""  default:
    fun = 0;
    break;
  }
  Schedule s;
  s.type = ON_MESSAGE;
  Stream * str = add_stream(in->output, out, s, fun, 0);
  connect(in, str);
  return str;
}"""
print(main_func)

        
