enums = { 'i8'  : 'int8_t',
          'i16' : 'int16_t',
          'i32' : 'int32_t',
          'i64' : 'int64_t',
          'u8'  : 'uint8_t',
          'u16' : 'uint16_t',
          'u32' : 'uint32_t',
          'u64' : 'uint64_t',
          'f32' : 'float',
          'f64' : 'double',
}


print('#include "basics.h"\n#include <stdio.h>\n#include <stdlib.h>\n\n')
print('/***')
print(' * AUTO GENERATED CODE')
print(' * gen_accumulate.py')
print(' ***/\n')



main_func = """Stream * accumulate(Stream *str)
{
  Stream *target;
  Schedule s;
  s.type = ON_MESSAGE;
  void * payload;
  switch (str->output) {
"""
for enum in enums:
    t = enums[enum]
    func = "Message * _acc_"+enum+"(Stream *str, Message *msg)\n"
    func += "{\n"
    func += "  "+t+"* acc = str->payload;\n"
    func += "  "+t+"* val = msg->payload;\n"
    func += "  *acc += *val;\n"
    func += "  Message * out = malloc(sizeof(Message));\n"
    func += "  out->payload = acc;\n"
    func += "  out->source = str;\n"
    func += "  out->type = str->output;\n"
    func += "  return out;\n"
    func += "}\n";
    print(func)
    main_func += "  case "+enum+": \n"
    main_func += "    payload = malloc(sizeof("+t+"));\n"
    main_func += "    *("+t+"*)payload = 0;\n"
    main_func += "    target = add_stream(str->output, str->output, s, &_acc_"+enum+", payload);\n"
    main_func += "    break;\n"
main_func += """  default:
    target = 0;
    break;
  }
  if (target)
    connect(str, target);
  return target;
}"""
print(main_func);
    
