enums = { 'i8'  : 'int8_t',
          'i16' : 'int16_t',
          'i32' : 'int32_t',
          'i64' : 'int64_t',
          'u8'  : 'uint8_t',
          'u16' : 'uint16_t',
          'u32' : 'uint32_t',
          'u64' : 'uint64_t',
          'f32' : 'float',
          'f64' : 'double',
          'BOOL': 'bool',
          'CHAR': 'char',
}

print_type = { 'i8'  : '%d',
          'i16' : '%d',
          'i32' : '%d',
          'i64' : '%ld',
          'u8'  : '%u',
          'u16' : '%u',
          'u32' : '%u',
          'u64' : '%lu',
          'f32' : '%f',
          'f64' : '%f',
          'BOOL': '%u',
          'CHAR': '%c',
}

print('#include "basics.h"\n#include <stdio.h>\n#include <stdlib.h>\n\n')
print('/***')
print(' * AUTO GENERATED CODE')
print(' * gen_print.py')
print(' ***/\n')

def_func = """Message * _print_any(Stream *str, Message *msg)
{
  printf("??: %p\\n", msg->payload);
  Message * out = new(Message);
  out->payload = msg->payload;
  out->type = msg->type;
  out->source = str;
  return out;
}\n"""

print(def_func);

main_func = """Stream * printstr(Stream *str)
{
  Stream *target;
  Schedule s;
  s.type = ON_MESSAGE;
  switch (str->output) {
"""
for enum in enums:
    func = "Message * _print_"+enum+"(Stream *str, Message *msg)\n"
    func += "{\n"
    func += "  printf(\""+enum+": "+print_type[enum]+"\\n\", *("+enums[enum]+"*)msg->payload);\n"
    func += "  Message * out = new(Message);\n"
    func += "  out->payload = msg->payload;\n"
    func += "  out->source = str;\n"
    func += "  out->type = msg->type;\n"
    func += "  return out;\n"
    func += "}\n";
    print(func)
    main_func += "  case "+enum+": \n"
    main_func += "    target = add_stream(str->output, str->output, s, &_print_"+enum+", 0);\n"
    main_func += "    break;\n"
main_func += """  default:
    target = add_stream(str->output, str->output, s, &_print_any, 0);
    break;
  }
  connect(str, target);
  return target;
}"""
print(main_func);
    
