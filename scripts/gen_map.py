enums = { 'i8'  : 'int8_t',
          'i16' : 'int16_t',
          'i32' : 'int32_t',
          'i64' : 'int64_t',
          'u8'  : 'uint8_t',
          'u16' : 'uint16_t',
          'u32' : 'uint32_t',
          'u64' : 'uint64_t',
          'f32' : 'float',
          'f64' : 'double',
          'BOOL': 'bool',
          'CHAR': 'char',
}


print('#include "basics.h"\n#include <stdlib.h>\n#include <stdint.h>\n\n')
print('/***')
print(' * AUTO GENERATED CODE')
print(' * gen_map.py')
print(' ***/\n')

for a in enums:
    get_func = 'void * _get_'+a+'_func(dtype output)\n{\n'
    get_func += '  switch (output) {\n'
    for b in enums:
        name = '_exec_'+a+'_'+b
        get_func += '    case '+b+': return &'+name+';\n'
        exe_func = 'Message * '+name+'(Stream *str, Message *data)\n{\n'
        exe_func += '  ' + enums[a] + '  val = *('+enums[a]+'*)data->payload;\n'
        exe_func += '  ' + enums[b] + ' (*fn)('+enums[a]+') = str->payload;\n'
        exe_func += '  ' + enums[b] + ' *res = new('+enums[b]+');\n'
        exe_func += '  *res = fn(val);\n'
        exe_func += '  Message * msg = new(Message);\n'
        exe_func += '  msg->payload = res;\n'
        exe_func += '  msg->source = str;\n'
        exe_func += '  msg->type = str->output;\n\n  return msg;\n}\n'
        print(exe_func)
    get_func += '    default: return 0;\n  }\n}\n'
    print(get_func)


map_func = '''
Stream * map        (Stream *  str,
                     void   *  fun,
                     dtype     type)
{
  void * func = 0;
  switch (str->output) {
'''
for k in enums:
    map_func += '  case '+k+':\n    func = _get_'+k+'_func(type);\n    break;\n'

map_func += '''  default:
    break;
  }
  Schedule s;
  s.type = ON_MESSAGE;
  Stream *target = add_stream(str->output, type, s, func, fun);
  connect(str, target);
  return target;
}\n
'''

print(map_func)
map_func = '''
Stream * filter     (Stream *  str,
                     void          *  fun)
{
  void * func = 0;
  switch (str->output) {
'''
for k in enums:
    t = enums[k]
    filter_func = 'Message * _filter_'+k+'_type(Stream * str, Message * msg)\n{\n'
    filter_func += '  '+t+'* val = msg->payload;\n'
    filter_func += '  bool (*fn)('+t+') = str->payload;\n'
    filter_func += '  if (fn(*val)) {\n'
    filter_func += '    Message * out = new(Message);\n'
    filter_func += '    out -> payload = val;\n'
    filter_func += '    out -> type = str->output;\n'
    filter_func += '    out -> source = str;\n'
    filter_func += '    return out;\n'
    filter_func += '  }\n'
    filter_func += '  return 0;\n}\n'
    print(filter_func)
    map_func += '  case '+k+':\n    func = &_filter_'+k+'_type;\n    break;\n'

map_func += '''  default:
    break;
  }
  Schedule s;
  s.type = ON_MESSAGE;
  Stream *target = add_stream(str->output, str->output, s, func, fun);
  connect(str, target);
  return target;
}
'''


print(map_func)

      
